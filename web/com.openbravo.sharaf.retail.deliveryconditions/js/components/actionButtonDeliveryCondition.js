/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'OB.OBPOSPointOfSale.UI.EditLine.SharafDeliveryConditionButton',
  kind: 'OB.UI.SmallButton',
  classes: 'btnlink-orange',
  handlers: {
    onSetMultiSelected: 'setMultiSelected'
  },
  tap: function () {
    var me = this;
    OB.MobileApp.view.waterfall('onShowPopup', {
      popup: 'CUSTDEL_UI_DeliveryConditionsPopup',
      args: {
        selectedModels: me.owner.owner.selectedModels
      }
    });
  },
  init: function (model) {
    this.model = model;
    this.model.get('order').on('change:isPaid change:isEditable', function (newValue) {
      if (newValue) {
        if (newValue.get('isPaid') === true || newValue.get('isEditable') === false) {
          this.setShowing(false);
          return;
        }
      }
      this.setShowing(true);
    }, this);
    this.model.get('order').on('change:custsdtDocumenttypeSearchKey', function (receipt) {
      // If 'Brand Promise' document type is selected, clear all delivery types of current lines
      if (receipt.get('custsdtDocumenttypeSearchKey') === 'BP') {
        _.each(receipt.get('lines').models, function (line) {
          if (line.get('cUSTDELDeliveryCondition') !== 'H' && line.get('cUSTDELDeliveryCondition') !== 'P' && line.get('cUSTDELDeliveryCondition') !== 'IM') {
            line.unset('cUSTDELDeliveryCondition');
            line.unset('cUSTDELDeliveryTime');
          }
        });
      } else {
        // If any document type different to 'Brand Promise' is selected, check if there are any 'Home Delivery' or 'Store Pickup' lines. If so, clear all delivery types of current lines
        var bpLines = _.find(receipt.get('lines').models, function (line) {
          return line.get('cUSTDELDeliveryCondition') === 'H' || line.get('cUSTDELDeliveryCondition') === 'P';
        });
        if (bpLines) {
          _.each(receipt.get('lines').models, function (line) {
            if (!line.get('originalOrderLineId')) {
              line.unset('cUSTDELDeliveryCondition');
              line.unset('cUSTDELDeliveryTime');
            }
          });
        }
      }
    }, this);
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTDEL_DeliveryCondition'));
    if (OB.MobileApp.model.hasPermission('CUSTDEL_DeliveryConditionsButton', true)) {
      this.show();
    } else {
      this.hide();
    }
  },
  // hide 'Delivery Type' button if there are multiple products selected and following conditions are met:
  // * 'Warehouse Delivery' & 'Supplier Delivery'
  // * 'Warehouse Delivery' & Any other type
  // * 'Supplier Delivery' & Any other type
  setMultiSelected: function (inSender, inEvent) {
    var me = this,
        showDeliveryButton = false;
    _.each(inEvent.models, function (line) {
      if (inEvent.models.length > 1){
    	  showDeliveryButton = false;
      } else {
    	  showDeliveryButton = true;
      }
    });
    if (!showDeliveryButton) {
      me.setShowing(false);
    } else {
      me.setShowing(true);
    }
    return;
  }
});

OB.OBPOSPointOfSale.UI.EditLine.prototype.actionButtons.push({
  kind: 'OB.OBPOSPointOfSale.UI.EditLine.SharafDeliveryConditionButton',
  name: 'sharafDeliveryConditionButton'
});