/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */
 
enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsPopup',
  kind: 'OB.UI.ModalAction',
  i18nHeader: 'CUSTDEL_SelectDelivery',
  handlers: {
    onApplyChanges: 'applyChanges',
    onSetValue: 'setValue'
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      name: 'deliveryConditionApplyButton',
      kind: 'CUSTDEL.UI.DeliveryConditionsApply'
    }, {
      name: 'deliveryConditionCancelButton',
      kind: 'CUSTDEL.UI.DeliveryConditionsCancel'
    }]
  },
  executeOnShow: function () {
    this.waterfall('onLoadValue');
    if (this.args || (this.args && this.args.mandatory === true)) {
      this.autoDismiss = false;
      this.closeOnEscKey = false;
      this.$.headerCloseButton.hide();
    } else {
      this.autoDismiss = true;
      this.closeOnEscKey = true;
      this.$.headerCloseButton.show();
    }
  },
  applyChanges: function (inSender, inEvent) {
    var selectedDelivery = this.$.bodyContent.$.attributes.$.line_deliveryConditionsBox.$.newAttribute.$.deliveryConditionsBox.$.renderCombo.getValue();
    var selectedDeliveryLoc = this.$.bodyContent.$.attributes.$.line_deliveryLocationsBox.$.newAttribute.$.deliveryLocationsBox.$.renderCombo.getValue();
    OB.MobileApp.model.set('selectedDel', selectedDelivery);
    OB.MobileApp.model.set('selectedLoc', selectedDeliveryLoc);
    this.waterfall('onApplyChange', inEvent);
    return inEvent;

  },
  setValue: function (inSender, inEvent) {

    var i, j;
    for (j in inEvent.data) {
      if (inEvent.data.hasOwnProperty(j)) {
        for (i = 0; i < this.args.selectedModels.length; i++) {
          this.args.selectedModels[i].set(j, inEvent.data[j]);
        }
      }
    }
    return true;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsPopupImpl',
  kind: 'CUSTDEL.UI.DeliveryConditionsPopup',
  newAttributes: [{
    kind: 'CUSTDEL.UI.DeliveryConditionsBox',
    name: 'deliveryConditionsBox',
    i18nLabel: 'CUSTDEL_DeliveryCondition'
  }, {
    kind: 'CUSTDEL.UI.DeliveryLocationsBox',
    name: 'deliveryLocationsBox',
    i18nLabel: 'CUSTDEL_DeliveryLocation'
  }, {
    kind: 'CUSTDEL.UI.ReceiptDeliveryDate',
    name: 'receiptDeliveryDate',
    i18nLabel: 'CUSTDEL_DeliveryDate'
  }, {
    kind: 'CUSTDEL.UI.ReceiptDeliveryTime',
    name: 'receiptDeliveryTime',
    i18nLabel: 'CUSTDEL_DeliveryTime'
  }]
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsBox',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  handlers: {
    onchange: 'selectChanged'
  },
  modelProperty: 'cUSTDELDeliveryCondition',
  retrievedPropertyForValue: 'searchKey',
  retrievedPropertyForText: 'name',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    for (i; i < OB.MobileApp.model.get('sharafDeliveryConditions').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    this.setDeliveryConditionsCollection(inSender, inEvent);
    if (inSender.args.selectedModels.length >= 1) {
      this.$.renderCombo.setSelected(0);
      if (inSender.args.selectedModels[0].get(this.modelProperty)) {
        var i = 0;
        for (i; i < this.collection.length; i++) {
          if (this.collection.models[i].get('searchKey') === inSender.args.selectedModels[0].get(this.modelProperty)) {
            this.$.renderCombo.setSelected(i);
            break;
          }
        }
      }
    } else {
      this.$.renderCombo.setSelected(0);
    }
  },
  selectChanged: function (inSender, inEvent) {
    var id = inSender.getValue();
    var me = this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.$.newAttribute.$.deliveryLocationsBox;
    this.setNewDeliveryLocationsCollection(me, id);
    me.$.renderCombo.setSelected(0);
    if (me.collection.length > 0) {
      this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(true);
      var i = 0;
      for (i; i < me.collection.length; i++) {
        if (me.collection.models[i].get('warehousename')) {
          me.$.renderCombo.setSelected(i);
          break;
        }
      }
    } else {
      this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(false);
    } 
    if(id === 'SD'){  
    	async function onchangeAsync(inSender) {
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timeOfDayPicker.setSelected(this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timeOfDayPicker.getClientControls()[0]);
await this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.fetchTimesAndSetValue();
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timeOfDay.applyStyle('display', 'flex !important');
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timePicker.setDisabled(true);
    	var org = OB.MobileApp.model.get('terminal').organization;
    	const { weekendDayName, publicHolidays } = await this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryDate.$.newAttribute.$.receiptDeliveryDate.getWeekendAndHolidays(org);
    	var currentDate = new Date();
    	var futureDate =this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryDate.$.newAttribute.$.receiptDeliveryDate.addBusinessDaysSkippingWeekendsAndHolidays(currentDate, 2, weekendDayName, publicHolidays);
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryDate.$.newAttribute.$.receiptDeliveryDate.setValue(futureDate);
    	}onchangeAsync.call(this, inSender);
    	    }else{
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timePicker.setValue(new Date());
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timeOfDay.applyStyle('display', 'none !important');
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryTime.$.newAttribute.$.receiptDeliveryTime.$.timePicker.setDisabled(false);
    	this.owner.owner.owner.owner.owner.attributeContainer.$.line_receiptDeliveryDate.$.newAttribute.$.receiptDeliveryDate.setValue(new Date());
    	}
  },

  setNewDeliveryLocationsCollection: function (me, id) {
    var wdDelObj, sdDelObj, imDelObj, hDelObj, pDelObj, cdDelObj, ddDelObj, ccDelObj, onDelObj;
    var orgWarehouseLocationObj = JSON.parse(localStorage.getItem('orgWarehouseLocation')).WarehouseLocation;

    if (id === 'WD') {
      wdDelObj = orgWarehouseLocationObj["WD"];
    }
    if (id === 'SD') {
      sdDelObj = orgWarehouseLocationObj["SD"];
    }
    if (id === 'IM') {
      imDelObj = orgWarehouseLocationObj["IM"];
    }
    if (id === 'H') {
      hDelObj = orgWarehouseLocationObj["H"];
    }
    if (id === 'P') {
      pDelObj = orgWarehouseLocationObj["P"];
    }
    if (id === 'DD') {
      cdDelObj = orgWarehouseLocationObj["DD"];
    }
    if (id === 'OD') {
      ddDelObj = orgWarehouseLocationObj["OD"];
    }
    if (id === 'C') {
      ccDelObj = orgWarehouseLocationObj["C"];
    }
    if (id === 'ON') {
      onDelObj = orgWarehouseLocationObj["ON"];
    }

    me.$.renderCombo.collection.reset(null);
    me.collection = new Backbone.Collection();
    me.$.renderCombo.setCollection(me.collection);
    if (!OB.UTIL.isNullOrUndefined(id)) {
      if (id === 'WD') {
        if (!OB.UTIL.isNullOrUndefined(wdDelObj)) {
          var wdLoc = wdDelObj.split(",");
          for (var i = 0; i < wdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = wdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'SD') {
        if (!OB.UTIL.isNullOrUndefined(sdDelObj)) {
          var sdLoc = sdDelObj.split(",");
          for (var i = 0; i < sdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = sdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'IM') {
        if (!OB.UTIL.isNullOrUndefined(imDelObj)) {
          var imLoc = imDelObj.split(",");
          for (var i = 0; i < imLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = imLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'H') {
        if (!OB.UTIL.isNullOrUndefined(hDelObj)) {
          var hLoc = hDelObj.split(",");
          for (var i = 0; i < hLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = hLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'P') {
        if (!OB.UTIL.isNullOrUndefined(pDelObj)) {
          var pLoc = pDelObj.split(",");
          for (var i = 0; i < pLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = pLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'DD') {
        if (!OB.UTIL.isNullOrUndefined(cdDelObj)) {
          var cdLoc = cdDelObj.split(",");
          for (var i = 0; i < cdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = cdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'OD') {
        if (!OB.UTIL.isNullOrUndefined(ddDelObj)) {
          var ddLoc = ddDelObj.split(",");
          for (var i = 0; i < ddLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ddLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'C') {
        if (!OB.UTIL.isNullOrUndefined(ccDelObj)) {
          var ccLoc = ccDelObj.split(",");
          for (var i = 0; i < ccLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ccLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'ON') {
        if (!OB.UTIL.isNullOrUndefined(onDelObj)) {
          var onLoc = onDelObj.split(",");
          for (var i = 0; i < onLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = onLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      }
    }
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      inEvent.data[this.modelProperty] = selected.get(this.retrievedPropertyForValue);
      return inEvent.data;
    }
  },
  setDeliveryConditionsCollection: function (inSender, inEvent) {
    // Set available Delivery Types based on items definition and document types
    var wdLine = false,
        spLine = false,
        imLine = false,
        hLine = false,
        pLine = false,
        cdLine = false,
        ddLine = false,
        ccLine = false,
        onLine = false,
        otherTypeLine = false,
        productDelivery = false,
        overrideRetDelivery = false,
        prodDeliveryName = "",
        delivery = "",
        verifiedReturn = "",
        stcheckWDSD = false;

    _.each(inSender.args.selectedModels, function (line) {
		
      if (line.get('cUSTDELDeliveryCondition') === 'WD') {
        wdLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'SD') {
        spLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'IM') {
        imLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'H' && OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') === 'BP') {
        hLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'P' && OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') === 'BP') {
        pLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'DD') {
        cdLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'OD') {
        ddLine = true;
      } else if (line.get('isVerifiedReturn')) {
        if (line.get('originalDeliveryCondition') === 'WD') {
          wdLine = true;
        } else if (line.get('originalDeliveryCondition') === 'SD') {
          spLine = true;
        } else if (line.get('originalDeliveryCondition') === 'IM' || line.get('originalDeliveryCondition') === 'ON' || line.get('originalDeliveryCondition') === 'P' || line.get('originalDeliveryCondition') === 'H') {
          imLine = true;
        } else if (line.get('originalDeliveryCondition') === 'H') {
          hLine = true;
        } else if (line.get('originalDeliveryCondition') === 'P') {
          pLine = true;
        } else if (line.get('originalDeliveryCondition') === 'DD') {
          cdLine = true;
        } else if (line.get('originalDeliveryCondition') === 'OD') {
          ddLine = true;
        }
      } else {
        // if any delivery type is selected from the dropdown
        otherTypeLine = true;
        if (line.get('newDeliveryCondition') === 'WD') {
          wdLine = true;
        } else if (line.get('newDeliveryCondition') === 'SD') {
          spLine = true;
        } else if (line.get('newDeliveryCondition') === 'IM' || line.get('newDeliveryCondition') === 'ON' || line.get('newDeliveryCondition') === 'P' || line.get('newDeliveryCondition') === 'H') {
          imLine = true;
        } else if (line.get('newDeliveryCondition') === 'H') {
          hLine = true;
        } else if (line.get('newDeliveryCondition') === 'P') {
          pLine = true;
        } else if (line.get('newDeliveryCondition') === 'DD') {
          cdLine = true;
        } else if (line.get('newDeliveryCondition') === 'OD') {
          ddLine = true;
        } else if (line.get('originalDeliveryCondition') === 'C') {
          // Quantity increase or decrease in Bulk Sales
          if (line.get('product').get('cUSTDELDeliveryCondition') === 'WD') {
            wdLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'SD') {
            spLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'IM' || line.get('product').get('cUSTDELDeliveryCondition') === 'ON' || line.get('product').get('cUSTDELDeliveryCondition') === 'P' || line.get('product').get('cUSTDELDeliveryCondition') === 'H') {
            imLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'H') {
            hLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'P') {
            pLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'DD') {
            cdLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'OD') {
            ddLine = true;
          }
        }
      }
      if (line.get('cUSTDELDeliveryCondition') === line.get('product').get('cUSTDELDeliveryCondition')) {
        productDelivery = true;
      } else if (otherTypeLine && line.get('originalDeliveryCondition') === line.get('product').get('cUSTDELDeliveryCondition')) {
        productDelivery = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'C' && line.get('originalDeliveryCondition') === 'C' && (wdLine || spLine || imLine || hLine || pLine || cdLine || ddLine || onLine)) {
        productDelivery = true;
      }

      if (line.get('cUSTDELDeliveryCondition') === 'C' && line.get('originalDeliveryCondition') === 'C' && !(wdLine && spLine && imLine && hLine && pLine && cdLine && ddLine && onLine)) {
        ccLine = true;
      }

      prodDeliveryName = line.get('product').get('cUSTDELDeliveryCondition');
      delivery = line.get('originalDeliveryCondition');
      verifiedReturn = line.get('isVerifiedReturn');

      if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('generatedFromQuotation')) && OB.MobileApp.model.receipt.get('generatedFromQuotation')) {
        delivery = line.get('product').get('cUSTDELDeliveryCondition');
        if (!OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') === 'BP') {
          productDelivery = true;
        }
        if (line.get('cUSTDELDeliveryCondition') === 'ON' || line.get('cUSTDELDeliveryCondition') === 'P' || line.get('cUSTDELDeliveryCondition') === 'H') {
          imLine = true;
        }
      }

      if (!OB.UTIL.isNullOrUndefined(verifiedReturn) && verifiedReturn) {
        if (line.get('originalDeliveryCondition') === 'WD') {
          wdLine = true;
        } else if (line.get('originalDeliveryCondition') === 'SD') {
          spLine = true;
        }
      }
      
      // OBDEV-252
     
     if(line.get('cUSTDELDeliveryCondition') === 'SD'  
        && line.get('originalDeliveryCondition') === 'WD'
        && line.get('product').get('cUSTDELDeliveryCondition') === 'WD'
        && OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') !== 'DS'
        ) {
        stcheckWDSD = true;
      }
      
    });

    this.$.renderCombo.collection.reset(null);
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    var delTypeArr = JSON.parse(localStorage.getItem('allowedDelivery'));
    if (delTypeArr.includes(delivery) && !OB.UTIL.isNullOrUndefined(verifiedReturn) && verifiedReturn) {
      productDelivery = true;
    }

    if (!productDelivery) {
      for (var p = 0; p < OB.MobileApp.model.get('sharafDeliveryConditions').length; p++) {
        for (var q = 0; q < delTypeArr.length; q++) {
          if (OB.MobileApp.model.get('sharafDeliveryConditions')[p].searchKey === delTypeArr[q]) {
			// OBDEV -252
			
			if(stcheckWDSD) {
               if (delTypeArr[q] !== 'IM') {
                 this.model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[p]);
                  this.collection.add(this.model);
               }
            } else {
              this.model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[p]);
              this.collection.add(this.model);
            }
          }
        }
      }
    } else {
      var isCustomerCare = false,
          isOnlineDelivery = false,
          isSPickup = false,
          isHomeDelivery = false,
          isSupplierDelivery = false,
          
          isOnlineCC = false,
          isCCSPickup = false,
          isOnlineHome = false,
          isCCHome = false,
          isOnlineSPickup = false,
          isSPickupHome = false,
          
          isOnlineSPickupHome = false,
          isSPickupCCHome = false,
          isOnlineCCSPickup = false,
          isOnlineCCSPickupHome = false,
          
          isWhIMCC = false,
          isWhIM = false,
          isSupplierIMCC = false,
          isSupplierIM = false;

      if (delTypeArr.includes('C')) {
        isCustomerCare = true;
      }
      if (delTypeArr.includes('SD')) {
        isSupplierDelivery = true;
      }
      if (delTypeArr.includes('ON')) {
        isOnlineDelivery = true;
      }
      if (delTypeArr.includes('P')) {
        isSPickup = true;
      }
      if (delTypeArr.includes('H')) {
        isHomeDelivery = true;
      }
      if (delTypeArr.includes('C') && delTypeArr.includes('ON')) {
        isOnlineCC = true;
      }
      if (delTypeArr.includes('C') && delTypeArr.includes('P')) {
        isCCSPickup = true;
      }
      if (delTypeArr.includes('ON') && delTypeArr.includes('H')) {
        isOnlineHome = true;
      }
      if (delTypeArr.includes('CC') && delTypeArr.includes('H')) {
        isCCHome = true;
      }
      if (delTypeArr.includes('ON') && delTypeArr.includes('P')) {
        isOnlineSPickup = true;
      }
      if (delTypeArr.includes('P') && delTypeArr.includes('H')) {
        isSPickupHome = true;
      }
      if (delTypeArr.includes('ON') && delTypeArr.includes('P') && delTypeArr.includes('H')) {
        isOnlineSPickupHome = true;
      }
      if (delTypeArr.includes('P') && delTypeArr.includes('C') && delTypeArr.includes('H')) {
        isSPickupCCHome = true;
      }
      if (delTypeArr.includes('C') && delTypeArr.includes('ON') && delTypeArr.includes('P')) {
        isOnlineCCSPickup = true;
      }

      if (delTypeArr.includes('ON') && delTypeArr.includes('C') && delTypeArr.includes('P') && delTypeArr.includes('H')) {
        isOnlineCCSPickupHome = true;
      }

      if (wdLine && verifiedReturn) {
        if (delTypeArr.includes('WD') && delTypeArr.includes('IM') && delTypeArr.includes('C')) {
          isWhIMCC = true;
        }
        if (delTypeArr.includes('WD') && delTypeArr.includes('IM')) {
          isWhIM = true;
        }
      }
      if (spLine && verifiedReturn) {
        if (delTypeArr.includes('SD') && delTypeArr.includes('IM') && delTypeArr.includes('C')) {
          isSupplierIMCC = true;
        }
        if (delTypeArr.includes('SD') && delTypeArr.includes('IM')) {
          isSupplierIM = true;
        }
      }

      for (i; i < OB.MobileApp.model.get('sharafDeliveryConditions').length; i++) {
        if (hLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
            continue;
          }
        } else if (pLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
            continue;
          }
        } else if (wdLine && verifiedReturn) {
          if (isWhIMCC) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM') {
              continue;
            }
          } else if (isWhIM) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM') {
              continue;
            }
          } else if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD') {
            continue;
          }
        } else if (wdLine) {
		  if (isCustomerCare && isSupplierDelivery) {
			if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD') {
              continue;
            }  	
		  } else if (isSupplierDelivery || isCustomerCare) {
			 if (isSupplierDelivery) {
				 if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD') {
                 continue;
          		  }
			 } else if (isCustomerCare) {
	            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
	              continue;
	            }
           }  
          }else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD') {
            continue;
          }
        } else if (spLine && verifiedReturn) {
          if (isSupplierIMCC) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM') {
              continue;
            }
          } else if (isSupplierIM) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM') {
              continue;
            }
          }  else if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
           } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD') {
            continue;
          }
        } else if (spLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD') {
            continue;
          }
        } else if (imLine) {
          if (isOnlineCCSPickupHome) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (isOnlineSPickupHome) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (isSPickupCCHome) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (isOnlineCCSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isOnlineCC) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON') {
              continue;
            }
          } else if (isOnlineSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isCCSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isOnlineHome) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (isCCHome) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'CC' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (isSPickupHome) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (isOnlineDelivery) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON') {
              continue;
            }
          } else if (isSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isHomeDelivery) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM') {
            continue;
          }
        } else if (cdLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'DD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'DD') {
            continue;
          }
        } else if (ddLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'OD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'OD') {
            continue;
          }
        } else if (ccLine) {
          if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== prodDeliveryName) {
            continue;
          }
        }
        this.model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[i]);
        this.collection.add(this.model);
      }

    }
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryLocationsBox',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  modelProperty: 'cUSTDELDeliveryLocation',
  retrievedPropertyForValue: 'warehouseid',
  retrievedPropertyForText: 'warehousename',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    for (i; i < OB.MobileApp.model.get('warehouses').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    this.setDeliveryLocationsCollection(inSender, inEvent);
    if (inSender.args.selectedModels.length >= 1) {
      this.$.renderCombo.setSelected(0);
      if (OB.UTIL.isNullOrUndefined(inSender.args.selectedModels[0].get(this.modelProperty))) {
        inSender.args.selectedModels[0].set(this.modelProperty, inSender.args.selectedModels[0].id)
      }
      if (inSender.args.selectedModels[0].get(this.modelProperty) && this.collection.length > 0) {
        this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(true);
        var i = 0;
        for (i; i < this.collection.length; i++) {
          if (inSender.args.selectedModels[0].get(this.modelProperty).length > 32) {
            if (this.collection.models[i].get('warehouseid') === inSender.args.selectedModels[0].get(this.modelProperty).substring(10, 42)) {
              this.$.renderCombo.setSelected(i);
              break;
            }
          } else {
            if (this.collection.models[i].get('warehouseid') === inSender.args.selectedModels[0].get(this.modelProperty)) {
              this.$.renderCombo.setSelected(i);
              break;
            }
          }
        }
      } else {
        this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(false);
      }
    } else {
      this.$.renderCombo.setSelected(0);
    }
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      inEvent.data[this.modelProperty] = selected.get(this.retrievedPropertyForValue);
      return inEvent.data;
    } else if (OB.MobileApp.model.receipt.get('generatedFromQuotation')) {
      inSender.args.selectedModels[0].attributes.warehouse = OB.MobileApp.model.get('warehouses')[0].warehouseid;
      inSender.args.selectedModels[0].attributes.warehousename = OB.MobileApp.model.get('warehouses')[0].warehousename;
    }
  },
  setDeliveryLocationsCollection: function (inSender, inEvent) {

    var wdDelObj, sdDelObj, imDelObj, hDelObj, pDelObj, cdDelObj, ddDelObj, ccDelObj, onDelObj;
    var orgWarehouseLocationObj = JSON.parse(localStorage.getItem('orgWarehouseLocation')).WarehouseLocation;

    _.each(inSender.args.selectedModels, function (line) {
      if (line.get('cUSTDELDeliveryCondition') === 'WD') {
        wdDelObj = orgWarehouseLocationObj["WD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'SD') {
        sdDelObj = orgWarehouseLocationObj["SD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'IM') {
        imDelObj = orgWarehouseLocationObj["IM"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'H') {
        hDelObj = orgWarehouseLocationObj["H"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'P') {
        pDelObj = orgWarehouseLocationObj["P"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'DD') {
        cdDelObj = orgWarehouseLocationObj["DD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'OD') {
        ddDelObj = orgWarehouseLocationObj["OD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'C') {
        ccDelObj = orgWarehouseLocationObj["C"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'ON') {
        onDelObj = orgWarehouseLocationObj["ON"];
      }
    });

    this.$.renderCombo.collection.reset(null);
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    if (!OB.UTIL.isNullOrUndefined(inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition)) {
      if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'WD') {
        if (!OB.UTIL.isNullOrUndefined(wdDelObj)) {
          var wdLoc = wdDelObj.split(",");
          for (var i = 0; i < wdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = wdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (!OB.UTIL.isNullOrUndefined(loc)) {
                if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                  model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                  this.collection.add(model);
                }
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'SD') {
        if (!OB.UTIL.isNullOrUndefined(sdDelObj)) {
          var sdLoc = sdDelObj.split(",");
          for (var i = 0; i < sdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = sdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'IM') {
        if (!OB.UTIL.isNullOrUndefined(imDelObj)) {
          var imLoc = imDelObj.split(",");
          for (var i = 0; i < imLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = imLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'H') {
        if (!OB.UTIL.isNullOrUndefined(hDelObj)) {
          var hLoc = hDelObj.split(",");
          for (var i = 0; i < hLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = hLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'P') {
        if (!OB.UTIL.isNullOrUndefined(pDelObj)) {
          var pLoc = pDelObj.split(",");
          for (var i = 0; i < pLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = pLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'DD') {
        if (!OB.UTIL.isNullOrUndefined(cdDelObj)) {
          var cdLoc = cdDelObj.split(",");
          for (var i = 0; i < cdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = cdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'OD') {
        if (!OB.UTIL.isNullOrUndefined(ddDelObj)) {
          var ddLoc = ddDelObj.split(",");
          for (var i = 0; i < ddLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ddLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'C') {
        if (!OB.UTIL.isNullOrUndefined(ccDelObj)) {
          var ccLoc = ccDelObj.split(",");
          for (var i = 0; i < ccLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ccLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'ON') {
        if (!OB.UTIL.isNullOrUndefined(onDelObj)) {
          var onLoc = onDelObj.split(",");
          for (var i = 0; i < onLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = onLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      }
    }
  }
});

enyo.kind({
	  name: 'CUSTDEL.UI.ReceiptDeliveryDate',
	  kind: 'onyx.DatePicker',
	  modelProperty: 'cUSTDELDeliveryTime',
	  events: {
	    onSetValue: ''
	  },
	  handlers: {
	    onLoadValue: 'loadValue',
	    onApplyChange: 'applyChange'
	  },

	  getWeekendAndHolidays: function (org) {
	    return new Promise((resolve, reject) => {
	      var query = "select s.* from custdel_skipdeliverydates s where s.active = 'true' and s.organization = '" + org + "'";
	      OB.Dal.queryUsingCache(OB.Model.preferredDeliveryDates, query, [], function (success) {
	        if (success.models.length > 0) {
	          var weekend = success.models[0].get('weekend');
	          const dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
	          var weekendDayName = dayNames[weekend - 1];
	          var id = success.models[0].get('id');
	          var holidayQuery = "select s.* from custdel_pubicholiday s where s.active = 'true' and s.headerID = '" + id + "'";
	          OB.Dal.queryUsingCache(OB.Model.publicHolidayDates, holidayQuery, [], function (holiday) {
	            var publicHolidays = [];
	            if (holiday.models.length > 0) {
	              for (var i = 0; i < holiday.models.length; i++) {
	                publicHolidays.push(new Date(holiday.models[i].get('dateOfHoliday')));
	              }
	            }
	            resolve({ weekendDayName, publicHolidays });
	          });
	        } else {
	          resolve({ weekendDayName: null, publicHolidays: [] });
	        }
	      });
	    });
	  },

	  isPublicHoliday: function (date, holidays) {
	    const dateString = date.toLocaleDateString('en-CA'); // This will format the date as 'YYYY-MM-DD'
	    var holidayString = holidays.map(holiday => holiday.toLocaleDateString('en-CA'));
	    return holidayString.includes(dateString);
	  },

	  addBusinessDaysSkippingWeekendsAndHolidays: function (startDate, daysToAdd, weekendDayName, holidays) {
	    let date = new Date(startDate);
	    let count = 0;
	    const weekendDayIndex = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].indexOf(weekendDayName);

	    while (count < daysToAdd) {
	      date.setDate(date.getDate() + 1);
	      if (date.getDay() !== weekendDayIndex && !this.isPublicHoliday(date, holidays)) {
	        count++;
	      }
	    }
	    return date;
	  },

	  loadValue: function (inSender, inEvent) {
	    var me = this;
	    var org = OB.MobileApp.model.get('terminal').organization;

	    async function processDates(inSender) {
	      const model = inSender.args.selectedModels[0];
	      const condition = model.get(this.modelProperty);
	      if (model.attributes.cUSTDELDeliveryCondition === 'SD') {
	        const { weekendDayName, publicHolidays } = await me.getWeekendAndHolidays(org);
	        var currentDate = new Date();
	        var futureDate = me.addBusinessDaysSkippingWeekendsAndHolidays(currentDate, 2, weekendDayName, publicHolidays);
	        me.setValue(futureDate);
	      } else if (condition) {
	        me.setValue(new Date(condition));
	      } else {
	        me.setValue(new Date());
	      }
	    }

	    processDates.call(this, inSender);
	  },

	  applyChange: function (inSender, inEvent) {
	    inEvent.data[this.modelProperty] = this.getValue();
	    return inEvent.data;
	  }
	});


enyo.kind({
	  name: 'CUSTDEL.UI.ReceiptDeliveryTime',
	  kind: 'enyo.FittableColumns', // Use FittableColumns for flex layout
	  style: 'display: flex; align-items: center; justify-content: center;',
	  components: [
	    {
	      kind: 'onyx.PickerDecorator',
	      name: 'timeOfDay',
	      style: 'display: flex; align-items: center; justify-content: center;', // Adjust the margin between components
	      components: [
	        { kind: 'onyx.PickerButton', content: 'Select Time of Day' },
	        {
	          kind: 'onyx.Picker',
	          name: 'timeOfDayPicker',
	          showing: false, // Initially hidden, will be shown if condition is 'SD'
	          onChange: 'timeOfDayChanged',
	          components: [
	            { content: 'Morning', active: true },
	            { content: 'Afternoon' },
	            { content: 'Evening' }
	          ]
	        }
	      ]
	    },
	    {
	      kind: 'onyx.TimePicker',
	      name: 'timePicker',
	      style: 'display: flex; align-items: center; justify-content: center;' // Adjust the margin between components
	    }
	  ],
	  modelProperty: 'cUSTDELDeliveryTime',
	  events: {
	    onSetValue: ''
	  },
	  handlers: {
	    onLoadValue: 'loadValue',
	    onApplyChange: 'applyChange'
	  },

	  // The loadValue function is now async
	  loadValue: async function (inSender, inEvent) {
	    this.$.timePicker.setLocale(OB.MobileApp.model.get('terminal').language_string);
	    const isSD = inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'SD';

	    if (isSD) {
	      this.$.timePicker.setDisabled(true);
	      this.$.timeOfDay.applyStyle('display', 'flex !important');
	    } else {
	      this.$.timePicker.setDisabled(false);
	      this.$.timeOfDay.applyStyle('display', 'none !important');
	    }

	    if (isSD && inSender.args.selectedModels.length === 1) {
	      // Set a default value if nothing is selected
	      if (!this.$.timeOfDayPicker.getSelected()) {
	        this.$.timeOfDayPicker.setSelected(this.$.timeOfDayPicker.getClientControls()[0]); // Set to "Morning" by default
	      }
	      await this.fetchTimesAndSetValue(); // Await the fetchTimesAndSetValue function
	    } else if (inSender.args.selectedModels.length === 1) {
	      if (inSender.args.selectedModels[0].get(this.modelProperty)) {
	        this.setValue(new Date(inSender.args.selectedModels[0].get(this.modelProperty)));
	      } else {
	        this.$.timePicker.setValue(new Date());
	      }
	    } else {
	      this.$.timePicker.setValue(new Date());
	    }
	  },

	  fetchTimesAndSetValue: function () {
	    return new Promise((resolve, reject) => {
	      var org = OB.MobileApp.model.get('terminal').organization;
	      var query = "select s.* from custdel_skipdeliverydates s where s.active = 'true' and s.organization = '" + org + "'";

	      OB.Dal.queryUsingCache(OB.Model.preferredDeliveryDates, query, [], enyo.bind(this, function (success) {
	        if (success.models.length > 0) {
	          var morningHour = success.models[0].get('morning');  // e.g., "10"
	          var afternoonHour = success.models[0].get('afternoon');  // e.g., "14"
	          var eveningHour = success.models[0].get('evening');  // e.g., "19"

	          var timeMapping = {
	            Morning: { hour: morningHour, minute: 0, period: 'AM' },
	            Afternoon: { hour: afternoonHour, minute: 0, period: 'PM' },
	            Evening: { hour: eveningHour, minute: 0, period: 'PM' }
	          };

	          // Store the time mapping for future use
	          this.timeMapping = timeMapping;

	          // Set the value based on the selected time of day
	          this.setValueBasedOnTimeOfDay();

	          resolve(timeMapping);  // Resolve the Promise with the time mapping
	        } else {
	          // Handle case where no data is returned
	          this.timeMapping = null;
	          this.showNoDataError();

	          resolve(null);  // Resolve with null to indicate no data was returned
	        }
	      }));
	    });
	  },

	  setValueBasedOnTimeOfDay: function () {
	    const timeMapping = this.timeMapping;  // Use the stored timeMapping

	    if (!timeMapping) {
	      console.log("Fetching Time mapping data....");
	      return;
	    }

	    const selectedTimeOfDay = this.$.timeOfDayPicker.getSelected() ? this.$.timeOfDayPicker.getSelected().getContent() : 'Morning';
	    const timeConfig = timeMapping[selectedTimeOfDay] || { hour: 0, minute: 0, period: 'AM' };
	    let date = new Date();

	    // Set hours based on the AM/PM period
	    let hours = parseInt(timeConfig.hour, 10);
	    if (timeConfig.period === 'PM' && hours < 12) {
	      hours += 12;
	    } else if (timeConfig.period === 'AM' && hours === 12) {
	      hours = 0; // Convert 12 AM to 00 hours
	    }

	    date.setHours(hours);
	    date.setMinutes(timeConfig.minute);

	    // Set the value on the time picker
	    this.$.timePicker.setValue(date);
	  },

	  timeOfDayChanged: function (inSender, inEvent) {
	    this.setValueBasedOnTimeOfDay();
	    this.$.timePicker.setDisabled(true);
	  },

	  applyChange: function (inSender, inEvent) {
	    var value = this.$.timePicker.getValue(),
	      deliveryTime = inEvent.data.cUSTDELDeliveryTime;

	    if (value) {
	      value.setSeconds(0);
	    }

	    inEvent.data[this.modelProperty] = new Date(deliveryTime.getFullYear(), deliveryTime.getMonth(), deliveryTime.getDate(), value.getHours(), value.getMinutes(), value.getSeconds());
	    return inEvent.data;
	  },

	  showNoDataError: function () {
	    OB.UTIL.showError("No delivery time data available. Please check your configuration.");
	  }
	});


enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsApply',
  kind: 'OB.UI.ModalDialogButton',
  events: {
    onHideThisPopup: '',
    onApplyChanges: '',
    onSetValue: ''
  },
  tap: function () {
	  var previousDeliCond,previousDeliDate;
	  OB.MobileApp.model.receipt.set('isDeliveryPopUpSelected',true);
		var selcetedLines = this.owner.owner.args.selectedModels[0];
        	if(!OB.UTIL.isNullOrUndefined(selcetedLines.get('cUSTDELDeliveryCondition'))){
           	 previousDeliCond = selcetedLines.get('cUSTDELDeliveryCondition');
        	}if(!OB.UTIL.isNullOrUndefined(selcetedLines.get('cUSTDELDeliveryTime'))){
           	 previousDeliDate = selcetedLines.get('cUSTDELDeliveryTime');
        	}
        var me = this,
        inEvent = {
        data: {}
        },
        callbackSuccess = function () {
        me.doSetValue(inEvent);
        me.model.get('order').save();
        me.model.get('orderList').saveCurrent();
        if (me.owner.owner.args.callback instanceof Function) {
          me.owner.owner.args.callback();
        }
        };
    this.doApplyChanges(inEvent);
    OB.UTIL.showLoading(true);

    if (!OB.UTIL.isNullOrUndefined(inEvent.data.cUSTDELDeliveryLocation)) {
      for (var i = 0; i < OB.MobileApp.model.get('warehouses').length; i++) {
        if (OB.MobileApp.model.get('warehouses')[i].warehouseid === inEvent.data.cUSTDELDeliveryLocation) {
          _.each(me.owner.owner.args.selectedModels, function (line) {
            line.set('cUSTDELDeliveryLocation', OB.MobileApp.model.get('warehouses')[i].warehousename);
          });
        }
      }
      _.each(me.owner.owner.args.selectedModels, function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.attributes.shaquoOriginalDocumentNo)) {
          line.attributes.warehouse = inEvent.data.cUSTDELDeliveryLocation;
          line.attributes.warehousename = me.owner.owner.args.selectedModels[0].get('cUSTDELDeliveryLocation');
        } else {
          line.attributes.warehouse.id = inEvent.data.cUSTDELDeliveryLocation;
          line.attributes.warehouse.warehousename = me.owner.owner.args.selectedModels[0].get('cUSTDELDeliveryLocation');
        }
      });
    } else {
      _.each(me.owner.owner.args.selectedModels, function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.attributes.shaquoOriginalDocumentNo)) {
          line.attributes.warehouse = OB.MobileApp.model.get('warehouses')[0].warehouseid;
          line.attributes.warehousename = OB.MobileApp.model.get('warehouses')[0].warehousename;
        } else {
          line.attributes.warehouse.id = OB.MobileApp.model.get('warehouses')[0].warehouseid;
          line.attributes.warehouse.warehousename = OB.MobileApp.model.get('warehouses')[0].warehousename;
        }
      });
    }

    _.each(me.owner.owner.args.selectedModels, function (line) {
      line.set('applyChanges', true);
    });

    var deliveryTypeSK = OB.MobileApp.model.get('selectedDel'),
        stockLocArr = OB.MobileApp.model.get('sharafStockLocation'),
        sharafDelivery = OB.MobileApp.model.get('sharafDeliveryConditions'),
        deliveryType = "",
        stockCheckLocation = "",
        warehouse = "";
    for (var i = 0; i < sharafDelivery.length; i++) {
      if (deliveryTypeSK === sharafDelivery[i].searchKey) {
        deliveryType = sharafDelivery[i].searchKey;
      }
    }
    for (var i = 0; i < stockLocArr.length; i++) {
      if (stockLocArr[i].organization === OB.MobileApp.model.attributes.terminal.organization$_identifier && deliveryType === stockLocArr[i].deliveryType) {
        stockCheckLocation = stockLocArr[i].stockLocation;
      }
    }

    if (_.isEmpty(stockCheckLocation)) {
      for (var i = 0; i < stockLocArr.length; i++) {
        if (deliveryType === stockLocArr[i].deliveryType && stockLocArr[i].organization === '*') {
          stockCheckLocation = stockLocArr[i].stockLocation;
          break;
        }
      }
    }

    getProductTotalQtyInReceiptForDelivery = function (currentLine, receipt, deliveryType) {
      var receiptQty = _.reduce(receipt.get('lines').models, function (memo, line, index) {
        if (line.get('id') !== currentLine.get('id') && line.get('product').get('id') === currentLine.get('product').get('id') && line.get('warehouse').id === currentLine.get('warehouse').id && line.get('cUSTDELDeliveryCondition') === deliveryType) {
          if (OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && line.get('product').get('productType') !== 'S') {
            return OB.DEC.add(memo, line.get('qty'));
          } else {
            return OB.DEC.add(memo, OB.DEC.Zero);
          }
        } else {
          return OB.DEC.add(memo, OB.DEC.Zero);
        }
      }, OB.DEC.Zero);
      return OB.DEC.add(receiptQty, currentLine.get('qty'));
    }

    getWarehouseDeliveryItemsWarehouse = _.find(OB.MobileApp.model.get('warehouses'), function (warehouse) {
      if (!OB.UTIL.isNullOrUndefined(warehouse.cUSTDELDeliveryType)) {
        return warehouse.cUSTDELDeliveryType === deliveryType;
      }
    });

    if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('selectedLoc')) && OB.MobileApp.model.get('selectedLoc').length > 0) {
      warehouse = OB.MobileApp.model.get('selectedLoc');
    } else if (!OB.UTIL.isNullOrUndefined(getWarehouseDeliveryItemsWarehouse)) {
      warehouse = getWarehouseDeliveryItemsWarehouse.warehouseid;
    }

    var productId = me.owner.owner.args.selectedModels[0].get('product').get('id'),
        qty = getProductTotalQtyInReceiptForDelivery(me.owner.owner.args.selectedModels[0], OB.MobileApp.model.receipt, deliveryType);;
           var validationOnDeliveryDateAndType = function (){
        	if(!OB.MobileApp.model.receipt.get('isVerifiedReturn')){
        	    var models = OB.MobileApp.model.receipt.get('lines').models;
        	    var isServiceProduct,previousMainArticleId,allowedDeliType,standardDeliveryDays,extendDeliverydays;
        	    var serviceLine = [];
        	    var currentDate = new Date();
        	    var deliveryDate = inEvent.data.cUSTDELDeliveryTime;
        	    var diffTime = (deliveryDate - currentDate);
        	    var differentDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        	    var productCategory;
        	    var productName;
        	    var orderlineId;
        	    var relatedLines = null;
        	    if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').AllowedDeliveryType)){
        	         allowedDeliType = OB.MobileApp.model.get('permissions').AllowedDeliveryType;
        	    }
        	    if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').StandardDeliveryDays)){
        	        standardDeliveryDays = +OB.MobileApp.model.get('permissions').StandardDeliveryDays;//15
        	    }
        	    if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').ExtendedDeliveryDays)){
        	        extendDeliverydays = +OB.MobileApp.model.get('permissions').ExtendedDeliveryDays;//30
        	    }
        	    for (var i = 0; i < models.length; i++) {
        	    	if(!OB.UTIL.isNullOrUndefined(models[i].get('relatedLines'))){
        	    		previousMainArticleId =models[i].get('relatedLines')[0].orderlineId;
        	    	    if(models[i].get('custdelIsdelart') && (previousMainArticleId == me.owner.owner.args.selectedModels[0].id)){
        	    	     isServiceProduct = true;
        	    	     if(!OB.UTIL.isNullOrUndefined(allowedDeliType) && !OB.UTIL.isNullOrUndefined(extendDeliverydays) && !OB.UTIL.isNullOrUndefined(standardDeliveryDays)){
        	    	     if((differentDays >standardDeliveryDays && differentDays<=extendDeliverydays) && allowedDeliType.includes(inEvent.data.cUSTDELDeliveryCondition)){
            	    		 models[i].set('custdelServiceRemove',false);
        	    	     }else if(differentDays > extendDeliverydays && allowedDeliType.includes(inEvent.data.cUSTDELDeliveryCondition)){
        	    	    	 models[i].set('custdelServiceRemove',false);
        	    	     }else{
            	    		 models[i].set('custdelServiceRemove',true);
        	    	     }
        	    	     }
            	    	 serviceLine.push(models[i]);
        	    	}
        	    }
           }
              if(!OB.UTIL.isNullOrUndefined(allowedDeliType) && !OB.UTIL.isNullOrUndefined(extendDeliverydays) && !OB.UTIL.isNullOrUndefined(standardDeliveryDays)){
        	    if(allowedDeliType.includes(inEvent.data.cUSTDELDeliveryCondition)){
        	    	if(differentDays > extendDeliverydays){
        	    		OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),OB.I18N.getLabel('CUSTDEL_ErrorMsgExtendedDeliverydate'));
        	    		me.owner.owner.args.selectedModels[0].set('cUSTDELDeliveryCondition',previousDeliCond);
        	    		me.owner.owner.args.selectedModels[0].set('cUSTDELDeliveryTime',previousDeliDate);
        	    		return false;
        	         }else if(differentDays >standardDeliveryDays && differentDays<=extendDeliverydays ){
        	        	 if(!isServiceProduct){
        	            	productCategory = selcetedLines.get('product').get('productCategory');
        	            	selcetedLines.set('hasRelatedServices' , true);
        	             	relatedLines = [{
        	            		 productCategory: selcetedLines.get('product').get('productCategory'),
        	                     productName : selcetedLines.get('product').get('description'),
        	                     orderlineId : selcetedLines.id,
        	                   }];
        	             	var query = "select pc.* from m_product_category pc where pc.m_product_category_id = '" + productCategory + "'";
        	                OB.Dal.queryUsingCache(OB.Model.ProductCategory, query, [], function (success) {
        	                 if(success.models.length > 0 && (success.models[0].get('custshaCategoryType') === 'Group')){
        	                        var query = "select p.* from m_product p where p.m_product_id in (select pc.custdelDeliservArti from m_product_category pc where pc.m_product_category_id = '" + productCategory + "')";
        	        				OB.Dal.queryUsingCache(OB.Model.Product, query, [], function(product) {
        	        	             if(product.models.length > 0){
        	        	            	 var prod = product.models[0];
        	        	            	 OB.MobileApp.model.receipt.addProductToOrder(prod, 1, undefined, {
        	        	                     originalLine: null,
        	        	                     custdisAddByPromo: true,
        	        	                     custdisPromotion: null,
        	        	                     custdisOrderline: null,
        	        	                     custdisIrType: null,
        	        	                     custdisRedemptionAmount: null,
        	        	                     custdisAmount: null,
        	        	                     cUSTDELDeliveryCondition: null,
        	        	                     cUSTDELDeliveryTime: null,
        	        	                     relatedLines: relatedLines,
        	        	                     custdelIsdelart: true
        	        	                   }, function () {
        	        	                   });
        	        	            	 OB.MobileApp.model.receipt.save();
        	        	             }
        	        				});
        	                       }else{
        	                    	   var parentId = "select pct.* from m_product_category_tree pct where pct.category_id = '" + productCategory + "'";
        	                    	   OB.Dal.queryUsingCache(OB.Model.ProductCategoryTree, parentId, [], function (success) {
        	                    		   if(success.models.length > 0){
        	                    			   var parent_id = success.models[0].get('parentId');
        	                    			   var query = "select p.* from m_product p where p.m_product_id in (select pc.custdelDeliservArti from m_product_category pc where pc.m_product_category_id = '" +  parent_id  + "')";
        	        	        				OB.Dal.queryUsingCache(OB.Model.Product, query, [], function(product) {
        	           	        	             if(product.models.length > 0){
        	           	        	            	 var prod = product.models[0];
        	           	        	            	 OB.MobileApp.model.receipt.addProductToOrder(prod, 1, undefined, {
        	           	        	                     originalLine: null,
        	           	        	                     custdisAddByPromo: true,
        	           	        	                     custdisPromotion: null,
        	           	        	                     custdisOrderline: null,
        	           	        	                     custdisIrType: null,
        	           	        	                     custdisRedemptionAmount: null,
        	           	        	                     custdisAmount: null,
        	           	        	                     cUSTDELDeliveryCondition: null,
        	           	        	                     cUSTDELDeliveryTime: null,
        	           	        	                     relatedLines: relatedLines,
        	           	        	                     custdelIsdelart: true
        	           	        	                   }, function () {
        	           	        	                   });
        	           	        	            	 OB.MobileApp.model.receipt.save();
        	           	        	             }
        	           	        				});
        	                    		   }
        	                    	   });
        	                    	   }
        	                       });
        	                }
        	         }else{
     					if (isServiceProduct) {
    			            OB.MobileApp.model.receipt.deleteLinesFromOrder(serviceLine);
    							}
    						} 
        	            }else{
            	    		if (isServiceProduct) {
        			            OB.MobileApp.model.receipt.deleteLinesFromOrder(serviceLine);
        							}
            	    	}
        	    	}
            	 //validation to restrict weekend and public holiday if delivery type is supplier delivery
          	if(!OB.UTIL.isNullOrUndefined(inEvent.data.cUSTDELDeliveryCondition) && inEvent.data.cUSTDELDeliveryCondition === 'SD'){
          		function formatDate(date) {
          		  const year = date.getFullYear();
          		  const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are 0-based
          		  const day = String(date.getDate()).padStart(2, '0');
          		  return `${year}-${month}-${day}`;
          		}
          		var isBackgroundPopup = false;
          		var holidayDes;
          		var currentDate = new Date();
          		var org = OB.MobileApp.model.get('terminal').organization;
          		var query = "select s.* from custdel_skipdeliverydates s where s.active = 'true' and s.organization = '" + org + "'";
          		OB.Dal.queryUsingCache(OB.Model.preferredDeliveryDates, query, [], function (success) {
          			if(success.models.length > 0){
          				var weekend = success.models[0].get('weekend');
          				const dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
          				var weekendDayName = dayNames[weekend - 1];
          				var id = success.models[0].get('id');
          				var selectedDate = inEvent.data.cUSTDELDeliveryTime;
          				var selectedDateWithoutTime = formatDate(selectedDate);
          				const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
          	        	const formattedDate = selectedDate.toLocaleDateString('en-US', options);
          	        	const selectedayName = selectedDate.toLocaleString('en-US', { weekday: 'long' });
          	        	const selectedday = selectedDate.getDate();
          					if(weekendDayName !== selectedayName){
          						var query = "select s.* from custdel_pubicholiday s where s.active = 'true' and s.headerID = '" + id + "'";
          		          		OB.Dal.queryUsingCache(OB.Model.publicHolidayDates, query, [], function (holiday) {
          		          			if(holiday.models.length > 0){
          		          				for(var i=0; i<holiday.models.length; i++){
          		          					var publicHolidayDate = new Date(holiday.models[i].get('dateOfHoliday'));    		          					
          		          					var publicHolidayDateWithoutTime = formatDate(publicHolidayDate);
          		          				    holidayDes = holiday.models[i].get('description');
          		          					if(selectedDateWithoutTime === publicHolidayDateWithoutTime){
          		                       	    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  OB.I18N.getLabel('CUSTDEL_Holiday',[selectedDateWithoutTime,holidayDes]));
          		          				    isBackgroundPopup = true;
          		          					return false;
          		          					}
          		          				}
          		          			}
          		          		});
          					}else{
		                       	OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  OB.I18N.getLabel('CUSTDEL_Weekend',[selectedDateWithoutTime,weekendDayName]));
		                       	isBackgroundPopup = true;
              			        return false;
          					}		
                 	 }
                  });     		
          	}
          	}
        	if(!isBackgroundPopup){
        		me.doHideThisPopup();
        	}
        	 };   	 
        if (!_.isEmpty(stockCheckLocation) && !OB.UTIL.isNullOrUndefined(warehouse) && warehouse.length > 0) {
      var process = new OB.DS.Request('com.openbravo.sharaf.retail.deliveryconditions.processes.CheckStockSAPService');
      process.exec({
        productId: productId,
        qty: qty,
        warehouse: warehouse,
        stockLocation: stockCheckLocation
      }, function (data) {
        if (data && data.exception) {
          OB.UTIL.showLoading(false);
          if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.exception) && !OB.UTIL.isNullOrUndefined(data.exception.status)
              && !OB.UTIL.isNullOrUndefined(data.exception.status.data) && !OB.UTIL.isNullOrUndefined(data.exception.status.data.message)) {
                 OB.UTIL.showConfirmation.display(data.exception.status.data.message, [{
                    label: OB.I18N.getLabel('OBMOBC_LblOk')
                 }]);
          } else {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
                  label: OB.I18N.getLabel('OBMOBC_LblOk')
               }]);      
          }
          me.owner.owner.args.selectedModels[0].set('isStockAvailable', false);
          return false;
        } else {
          OB.UTIL.showLoading(false);
          me.owner.owner.args.selectedModels[0].set('isStockAvailable', true);
          callbackSuccess();
          validationOnDeliveryDateAndType();
        }
      }, function () {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
          label: OB.I18N.getLabel('OBMOBC_LblOk')
        }]);
      });
    } else {
      me.owner.owner.args.selectedModels[0].set('isStockValidationNeeded', false);
      OB.UTIL.showLoading(false);
      callbackSuccess();
      validationOnDeliveryDateAndType();
    }

/* if (inEvent.data && (inEvent.data.cUSTDELDeliveryCondition === 'WD' || (OB.CUSTDEL.checkImmediateDeliveryItemsStock() && inEvent.data.cUSTDELDeliveryCondition === 'IM'))) {
      OB.UTIL.showLoading(true);
      var errors = [],
          checkStockInOB = OB.CUSTDEL.checkImmediateDeliveryItemsStock() && inEvent.data.cUSTDELDeliveryCondition === 'IM';
      var finalCheckReceiptStock = _.after(me.owner.owner.args.selectedModels.length, function () {
        if (errors.length > 0) {
          OB.UTIL.showLoading(false);
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), errors.join());
        } else {
          OB.UTIL.showLoading(false);
          callbackSuccess();
        }
      });
      _.each(me.owner.owner.args.selectedModels, function (line) {
        if (line.get('qty') > 0 && (!checkStockInOB || (checkStockInOB && OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && line.get('product').get('productType') !== 'S'))) {
          OB.CUSTDEL.checkStock(line.get('product').get('id'), OB.CUSTDEL.getProductTotalQtyInReceiptForWDorIM(line, OB.MobileApp.model.receipt, inEvent.data.cUSTDELDeliveryCondition), checkStockInOB, function () {
            finalCheckReceiptStock();
          }, function () {
            errors.push(line.get('product').get(OB.Constants.IDENTIFIER));
            finalCheckReceiptStock();
          });
        } else {
          finalCheckReceiptStock();
        }
      });
    } else {*/
    //  callbackSuccess();
    // }

  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
  },
  init: function (model) {
    this.model = model;
  }
});


enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsCancel',
  kind: 'OB.UI.ModalDialogButton',
  tap: function () {
	OB.MobileApp.model.receipt.set('isDeliveryPopUpSelected',false);
    var me = this;
    me.doHideThisPopup();
    if (!OB.UTIL.isNullOrUndefined(me.owner.owner.args.selectedModels[0].get('isStockAvailable')) && !me.owner.owner.args.selectedModels[0].get('isStockAvailable')) {
        var lines = [];
        var primaryLineid;
        for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
            line = OB.MobileApp.model.receipt.get('lines').models[i];
            if (line.get('custdisOffer') && line.productType !== 'S') {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisOrderline'))) {
                    primaryLineid = line.get('custdisOrderline');
                } else {
                    primaryLineid = line.id;
                }
            }
        }
        if (!OB.UTIL.isNullOrUndefined(primaryLineid) && (me.owner.owner.args.selectedModels[0].get('custdisOffer') || me.owner.owner.args.selectedModels[0].get('custdisOrderline'))) {
            lines = _.filter(OB.MobileApp.model.receipt.get('lines').models, function(line) {
                return line.get('custdisOrderline') === primaryLineid || line.id === primaryLineid;
            });
            OB.MobileApp.model.receipt.deleteLinesFromOrder(lines);
            return;
        } else {
           OB.MobileApp.model.receipt.deleteLinesFromOrder([me.owner.owner.args.selectedModels[0]]);
        }
   }
      if (me.owner.owner.args.callback instanceof Function) {
          me.owner.owner.args.callback();
      }
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblCancel'));
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTDEL.UI.DeliveryConditionsPopupImpl',
  name: 'CUSTDEL_UI_DeliveryConditionsPopup'
});  
