/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
	var publicHolidayDates = OB.Data.ExtensibleModel
			.extend({
				modelName : 'publicHolidayDates',
				tableName : 'custdel_pubicholiday',
				entityName : 'custdel_pubicholiday',
				source : 'com.openbravo.sharaf.retail.deliveryconditions.service.LoadPublicHoliday'
			});
	publicHolidayDates.addProperties([ {
		name : 'id',
		column : 'id',
		primaryKey : true,
		type : 'TEXT'
	}, {
		name : 'description',
		column : 'description',
		type : 'TEXT'
	},{
		name : 'organization',
		column : 'organization',
		primaryKey : false,
		type : 'TEXT'
	},{
		name : 'active',
		column : 'active',
		type : 'TEXT'
	},{
		name : 'headerID',
		column : 'headerID',
		type : 'TEXT'
	},{
		name : 'dateOfHoliday',
		column : 'dateOfHoliday',
		type : 'TEXT'
	}]);
	OB.Data.Registry.registerModel(publicHolidayDates);
	OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models
			.push(publicHolidayDates);
})();
