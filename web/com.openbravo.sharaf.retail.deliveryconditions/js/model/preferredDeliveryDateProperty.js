/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
	var preferredDeliveryDates = OB.Data.ExtensibleModel
			.extend({
				modelName : 'preferredDeliveryDates',
				tableName : 'custdel_skipdeliverydates',
				entityName : 'custdel_skipdeliverydates',
				source : 'com.openbravo.sharaf.retail.deliveryconditions.service.LoadPreferredDeliveryConditions'
			});
	preferredDeliveryDates.addProperties([ {
		name : 'id',
		column : 'id',
		primaryKey : true,
		type : 'TEXT'
	}, {
		name : 'organization',
		column : 'organization',
		primaryKey : false,
		type : 'TEXT'
	}, {
		name : 'weekend',
		column : 'weekend',
		type : 'TEXT'
	}, {
		name : 'deliveryType',
		column : 'deliveryType',
		type : 'TEXT'
	},
	 {
		name : 'morning',
		column : 'morning',
		type : 'TEXT'
	}, {
		name : 'afternoon',
		column : 'afternoon',
		type : 'TEXT'
	},{
		name : 'evening',
		column : 'evening',
		type : 'TEXT'
	},{
		name : 'active',
		column : 'active',
		type : 'TEXT'
	}]);
	OB.Data.Registry.registerModel(preferredDeliveryDates);
	OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models
			.push(preferredDeliveryDates);
})();
