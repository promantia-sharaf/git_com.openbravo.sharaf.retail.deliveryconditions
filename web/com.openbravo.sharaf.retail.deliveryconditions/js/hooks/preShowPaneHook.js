/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, _*/

OB.UTIL.HookManager.registerHook('OBPOS_PreShowPane', function(
    args,
    callbacks
  ) {
    if (
      !OB.UTIL.isNullOrUndefined(args.context.model) &&
      !OB.UTIL.isNullOrUndefined(args.context.model.attributes.order)
    ) {
      var order = args.context.model.attributes.order;
      var documentTypeId = order.attributes.custsdtDocumenttype;
      var query =
        "SELECT a.* from alloweddeltypes a where a.custsdt_documenttype_id ='" +
        documentTypeId +
        "'and a.active = 'true' order by a.priority asc";
      OB.Dal.queryUsingCache(OB.Model.AllowedDeliveryTypes, query, [], function(
        del
      ) {
        if (del.length > 0) {
          var deliveryArr = [];
          for (var i = 0; i < del.models.length; i++) {
            deliveryArr.push(del.models[i].attributes.custdelDeliverycondition);
          }
          localStorage.setItem('allowedDelivery', JSON.stringify(deliveryArr));
        }
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      });
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
  