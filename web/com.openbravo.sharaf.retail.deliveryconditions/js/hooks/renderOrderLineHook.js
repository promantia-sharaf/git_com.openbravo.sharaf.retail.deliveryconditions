/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.UTIL.HookManager.registerHook('OBPOS_RenderOrderLine', function (args, callbacks) {
  var cUSTDELDeliveryCondition = args.orderline.model.get('cUSTDELDeliveryCondition'),
      whForDeliveryTypeArr = [];

  for (var i = 0; i < OB.MobileApp.model.get('warehouses').length; i++) {
    if (OB.MobileApp.model.get('warehouses')[i].cUSTDELDeliveryType === cUSTDELDeliveryCondition) {
      whForDeliveryTypeArr.push(OB.MobileApp.model.get('warehouses')[i].warehouseid);
    }
  }

  if (!cUSTDELDeliveryCondition) {
    var cUSTDELDeliveryCondition = !OB.UTIL.isNullOrUndefined(args.orderline.model.get('product').get('cUSTDELDeliveryCondition')) ? args.orderline.model.get('product').get('cUSTDELDeliveryCondition') : 'IM';
    args.orderline.model.set('cUSTDELDeliveryCondition', cUSTDELDeliveryCondition);

    var prefDeliveryCondition = args.orderline.model.get('cUSTDELDeliveryCondition');
    var query = "SELECT a.* from alloweddeltypes a where a.custsdt_documenttype_id ='" + OB.MobileApp.model.receipt.get('custsdtDocumenttype') + "'and a.active = 'true' order by a.priority asc";
    OB.Dal.queryUsingCache(OB.Model.AllowedDeliveryTypes, query, [], function (del) {
      var isDelTypeAvailableInAllowed = false;
      for (var i = 0; i < del.models.length; i++) {
        if (args.orderline.model.get('cUSTDELDeliveryCondition') === del.models[i].attributes.custdelDeliverycondition) {
          prefDeliveryCondition = del.models[i].attributes.custdelDeliverycondition
          isDelTypeAvailableInAllowed = true
        }
      }

      if (!isDelTypeAvailableInAllowed) {
        //To take the 1st priority delivery type when product delivery type is not in allowed del types
        if (del.models.length > 1) {
          prefDeliveryCondition = del.models[0].get('custdelDeliverycondition');
        }
      }

      for (var i = 0; i < OB.MobileApp.model.get('warehouses').length; i++) {
        if (OB.MobileApp.model.get('warehouses')[i].cUSTDELDeliveryType === prefDeliveryCondition || (OB.MobileApp.model.get('warehouses')[i].warehouseid === args.orderline.model.get('warehouse').id && (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('isVerifiedReturn'))) && OB.MobileApp.model.receipt.get('isVerifiedReturn'))) {
          args.orderline.model.set('cUSTDELDeliveryLocation', OB.MobileApp.model.get('warehouses')[i].warehousename);
          args.orderline.model.attributes.warehouse.warehousename = OB.MobileApp.model.get('warehouses')[i].warehousename;
          args.orderline.model.attributes.warehouse.id = OB.MobileApp.model.get('warehouses')[i].warehouseid;
          break;
        } else {
          args.orderline.model.set('cUSTDELDeliveryLocation', OB.MobileApp.model.get('warehouses')[i].warehousename);
        }
      }
    });

  } else {
    // to set warehouse details of main article in secondary articles in promotion
    if (!args.orderline.model.get('cUSTDELDeliveryLocation') && !OB.UTIL.isNullOrUndefined(args.orderline.model.get('custdisOrderline'))) {
      for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
        var line = OB.MobileApp.model.receipt.get('lines').models;
        if (!OB.UTIL.isNullOrUndefined(line[i].get('custdisOffer')) && OB.UTIL.isNullOrUndefined(line[i].get('custdisOrderline'))) {
          if (line[i].get('cUSTDELDeliveryCondition') === args.orderline.model.set('cUSTDELDeliveryCondition')) {
            args.orderline.model.set('cUSTDELDeliveryLocation', line[i].get('warehouse').warehousename);
            args.orderline.model.attributes.warehouse.warehousename = line[i].get('warehouse').warehousename;
            args.orderline.model.attributes.warehouse.id = line[i].get('warehouse').id;
          }
        }
      }
      // to set delivery location in order line while converting Quotation to Invoice
    } else if (!OB.UTIL.isNullOrUndefined(args.orderline.model.get('shaquoOriginalDocumentNo'))) {
      if (OB.UTIL.isNullOrUndefined(args.orderline.model.get('applyChanges'))) {
        if (OB.MobileApp.model.receipt.attributes.custsdtDocumenttypeSearchKey === 'BP' || OB.MobileApp.model.receipt.attributes.custsdtDocumenttypeSearchKey === 'DP' || OB.MobileApp.model.receipt.attributes.custsdtDocumenttypeSearchKey === 'LS') {
          for (var i = 0; i < OB.MobileApp.model.get('warehouses').length; i++) {
            if (OB.MobileApp.model.get('warehouses')[i].cUSTDELDeliveryType === cUSTDELDeliveryCondition && (!whForDeliveryTypeArr.includes(args.orderline.model.attributes.warehouse))) {
              args.orderline.model.set('cUSTDELDeliveryLocation', OB.MobileApp.model.get('warehouses')[i].warehousename);
              args.orderline.model.attributes.warehousename = OB.MobileApp.model.get('warehouses')[i].warehousename;
              args.orderline.model.attributes.warehouse = OB.MobileApp.model.get('warehouses')[i].warehouseid;
              break;
            }
          }
        }
      }
    }

    var delivery = '',
        i = 0;
    for (i; i < OB.MobileApp.model.get('sharafDeliveryConditions').length; i++) {
      if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey === cUSTDELDeliveryCondition) {
        delivery = OB.MobileApp.model.get('sharafDeliveryConditions')[i].name;
        break;
      }
    }
    args.orderline.createComponent({
      style: 'float: left; width: 80%; display: block;',
      components: [{
        content: '-- ' + OB.I18N.getLabel('CUSTDEL_DeliveryCondition') + ': ' + delivery,
        attributes: {
          style: 'clear: left;'
        }
      }]
    });
  }
  if (args.orderline.model.get('isVerifiedReturn')) {
    for (var i = 0; i < OB.MobileApp.model.get('warehouses').length; i++) {
      if (OB.MobileApp.model.get('warehouses')[i].cUSTDELDeliveryType === args.orderline.model.get('cUSTDELDeliveryCondition')) {
        args.orderline.model.set('cUSTDELDeliveryLocation', OB.MobileApp.model.get('warehouses')[i].warehousename);
        args.orderline.model.get('warehouse').id = OB.MobileApp.model.get('warehouses')[i].warehouseid;
        args.orderline.model.get('warehouse').warehousename = OB.MobileApp.model.get('warehouses')[i].warehousename;
        break;
      }
    }
    if (args.orderline.model.get('cUSTDELDeliveryLocation').substring(10, 42) !== args.orderline.model.get('warehouse').warehousename) {
      args.orderline.model.set('cUSTDELDeliveryLocation', args.orderline.model.get('warehouse').warehousename);
    }
  }
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});
