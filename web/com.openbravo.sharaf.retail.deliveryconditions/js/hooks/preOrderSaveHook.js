OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function(args, callbacks) {

	if (args.receipt.get('isVerifiedReturn')){
		for (var i = 0; i < args.receipt.get('lines').models.length; i++) {
			var line = args.receipt.get('lines').models;
			 if (!OB.UTIL.isNullOrUndefined(line[i].get('cUSTDELDeliveryLocation'))){
				 var delLoc = line[i].get('cUSTDELDeliveryLocation').substring(10,42); 
				 for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
					 if(delLoc === OB.MobileApp.model.get('warehouses')[j].warehouseid){
						 line[i].get('warehouse').warehousename = OB.MobileApp.model.get('warehouses')[j].warehousename;
						 line[i].get('warehouse').id = OB.MobileApp.model.get('warehouses')[j].warehouseid;
					 }
		        }
			 }
		}
	}

    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});