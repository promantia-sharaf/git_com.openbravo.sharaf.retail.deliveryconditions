OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function(args, callbacks) {
     var order = args.context.get('order');

     for (var i = 0; i < order.get('lines').models.length; i++) {
            var line = order.get('lines').models;
          var olddt = line[i].get('cUSTDELDeliveryTime');
          var dt = OB.UTIL.isNullOrUndefined(line[i].get('cUSTDELDeliveryTime'))? new Date() : new Date(olddt);
          if(!OB.UTIL.isNullOrUndefined(line[i].get('cUSTDELDeliveryCondition')) && line[i].get('cUSTDELDeliveryCondition') === 'SD' && OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('isDeliveryPopUpSelected')) && !OB.MobileApp.model.receipt.get('isDeliveryPopUpSelected')){
        	  const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  		    var publicHolidays = [];
  		    var weekendDayName;
  		    var futureDate;
  		    var evg;
  		    var org = OB.MobileApp.model.get('terminal').organization;
  		    function getWeekendAndHolidays(org) {
  		        return new Promise((resolve, reject) => {
  		            var query = "select s.* from custdel_skipdeliverydates s where s.active = 'true' and s.organization = '" + org + "'";
  		            OB.Dal.queryUsingCache(OB.Model.preferredDeliveryDates, query, [], function (success) {
  		                if (success.models.length > 0) {
  		                    var weekend = success.models[0].get('weekend');
  		                    evg = success.models[0].get('evening');
  		                    const dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  		                    weekendDayName = dayNames[weekend - 1];
  		                    var id = success.models[0].get('id');
  		                    var holidayQuery = "select s.* from custdel_pubicholiday s where s.active = 'true' and s.headerID = '" + id + "'";
  		                    OB.Dal.queryUsingCache(OB.Model.publicHolidayDates, holidayQuery, [], function (holiday) {
  		                        if (holiday.models.length > 0) {
  		                            for (var i = 0; i < holiday.models.length; i++) {
  		                                publicHolidays.push(new Date(holiday.models[i].get('dateOfHoliday')));
  		                            }
  		                        }
  		                        resolve();
  		                    });
  		                } else {
  		                    resolve();
  		                }
  		            });
  		        });
  		    }
  		    function isPublicHoliday(date, holidays) {
  		        const dateString = date.toLocaleDateString('en-CA'); // This will format the date as 'YYYY-MM-DD'
  		        var holidayString = holidays.map(holiday => holiday.toLocaleDateString('en-CA'));
  		        return holidayString.includes(dateString);
  		    }
  		    function addBusinessDaysSkippingWeekendsAndHolidays(startDate, daysToAdd, weekendDayName, holidays) {
  		        let date = new Date(startDate);
  		        let count = 0;
  		        const weekendDayIndex = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].indexOf(weekendDayName);
  		        while (count < daysToAdd) {
  		            date.setDate(date.getDate() + 1);
  		            if (date.getDay() !== weekendDayIndex && !isPublicHoliday(date, holidays)) {
  		                count++;
  		            }
  		        }
  		        return date;
  		    }
  		  async function processDates(index) {
  			 await getWeekendAndHolidays(org);
  			futureDate = addBusinessDaysSkippingWeekendsAndHolidays(dt, 2, weekendDayName, publicHolidays);
  			if (futureDate.getHours().toString() == '0' 
                && futureDate.getMinutes().toString() == '0' 
                && futureDate.getSeconds().toString() == '0'){
  				futureDate.setHours(evg);
  				futureDate.setMinutes('0');
  				futureDate.setSeconds('0');
  				args.context.get('order').get('lines').models[index].set('cUSTDELDeliveryTime', futureDate);
  		        }		
  		  }
  		processDates(i);
          }
        }

    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});