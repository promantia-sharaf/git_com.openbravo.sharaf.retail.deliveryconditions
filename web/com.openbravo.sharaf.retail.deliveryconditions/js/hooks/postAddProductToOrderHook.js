/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, _*/

OB.UTIL.HookManager.registerHook('OBPOS_PostAddProductToOrder', function (args, callbacks) {
  var finalCallback = function () {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      },
      callbackSuccess = function () {
      var cUSTDELDeliveryCondition = !OB.UTIL.isNullOrUndefined(args.productToAdd.get('cUSTDELDeliveryCondition')) ? args.productToAdd.get('cUSTDELDeliveryCondition') : 'IM';
      args.orderline.set('cUSTDELDeliveryCondition', cUSTDELDeliveryCondition);
      if (!args.orderline.get('cUSTDELDeliveryTime')) {
        args.orderline.set('cUSTDELDeliveryTime', new Date());
      }
      finalCallback();
      },
          
      
      
      // checkStockInOB = OB.CUSTDEL.checkImmediateDeliveryItemsStock() && args.productToAdd.get('cUSTDELDeliveryCondition') === 'IM' && OB.UTIL.isNullOrUndefined(args.orderline.get('custdisOrderline')) && args.productToAdd.get('productType') !== 'S';
      checkStockInOB = args.productToAdd.get('cUSTDELDeliveryCondition') === 'IM' && OB.UTIL.isNullOrUndefined(args.orderline.get('custdisOrderline')) && args.productToAdd.get('productType') !== 'S';

  // Fetch allowed delivery types
  var documentTypeId = "",
      returnDoc = "";
  var sharafDocArr = OB.MobileApp.model.get('sharafDocumentType');
  for (i = 0; i < sharafDocArr.length; i++) {
    if (sharafDocArr[i].ticketSequence) {
      if (args.orderline && args.orderline.get('originalDocumentNo') && args.orderline.get('originalDocumentNo').startsWith(sharafDocArr[i].searchKey)) {
        returnDoc = sharafDocArr[i].preferredReturnDocumentType;
      }
    }
  }
  if (!_.isEmpty(returnDoc)) {
    for (i = 0; i < sharafDocArr.length; i++) {
      if (sharafDocArr[i].returnSequence && sharafDocArr[i].name === returnDoc) {
        documentTypeId = sharafDocArr[i].id;
      }
    }
  } else {
    documentTypeId = args.receipt.attributes.custsdtDocumenttype;
  }

  var query = "SELECT a.* from alloweddeltypes a where a.custsdt_documenttype_id ='" + documentTypeId + "'and a.active = 'true' order by a.priority asc";
  OB.Dal.queryUsingCache(OB.Model.AllowedDeliveryTypes, query, [], function (del) {
    if (del.length === 0) {
      finalCallback();
    } else {
      var deliveryArr = [];
      for (var i = 0; i < del.models.length; i++) {
        deliveryArr.push(del.models[i].attributes.custdelDeliverycondition);
      }

      args.orderline.set('originalDeliveryCondition', args.orderline.get('cUSTDELDeliveryCondition'));
      if (deliveryArr.length > 0 && !args.orderline.get('isVerifiedReturn') && deliveryArr.includes(args.productToAdd.get('cUSTDELDeliveryCondition'))) {
        if ((!args.receipt.get('custsdtDocumenttypeSearchKey') === 'LS') || (!args.receipt.get('custsdtDocumenttypeSearchKey') === 'DP')) {
          args.orderline.set('cUSTDELDeliveryCondition', args.productToAdd.get('cUSTDELDeliveryCondition'));
        }
      } else if (deliveryArr.length > 0 && args.orderline.get('isVerifiedReturn') && deliveryArr.includes(args.orderline.get('cUSTDELDeliveryCondition'))) {
        args.orderline.set('cUSTDELDeliveryCondition', args.orderline.get('cUSTDELDeliveryCondition'));
      } else {
        if (deliveryArr.length > 0) {
          args.orderline.set('cUSTDELDeliveryCondition', deliveryArr[0]);
        }
      }
      OB.MobileApp.model.receipt.save();
      localStorage.setItem('allowedDelivery', JSON.stringify(deliveryArr));

      var negativeQty = false;
      args.receipt.get('lines').models.forEach(function (line) {
        if (line.get('qty') < 0) {
          negativeQty = true;
        }
      });

      var salesDocTypesArr = [];
      for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
        var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
        if (docs.ticketSequence && (!docs.returnSequence) || docs.quotationSequence) {
          salesDocTypesArr.push(docs.searchKey);
        }
      }

      checkStockInSAP = function (args, finalCallback) {
        if (negativeQty) {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
          var deliveryTypeSK = args.orderline.get('cUSTDELDeliveryCondition'),
              stockLocArr = OB.MobileApp.model.get('sharafStockLocation'),
              sharafDelivery = OB.MobileApp.model.get('sharafDeliveryConditions'),
              deliveryType = "",
              stockCheckLocation = "";
          for (var i = 0; i < sharafDelivery.length; i++) {
            if (deliveryTypeSK === sharafDelivery[i].searchKey) {
              deliveryType = sharafDelivery[i].searchKey;
            }
          }
          for (var i = 0; i < stockLocArr.length; i++) {
            if (stockLocArr[i].organization === OB.MobileApp.model.attributes.terminal.organization$_identifier && deliveryType === stockLocArr[i].deliveryType) {
              stockCheckLocation = stockLocArr[i].stockLocation;
            }
          }

          if (_.isEmpty(stockCheckLocation)) {
            for (var i = 0; i < stockLocArr.length; i++) {
              if (deliveryType === stockLocArr[i].deliveryType && stockLocArr[i].organization === '*') {
                stockCheckLocation = stockLocArr[i].stockLocation;
                break;
              }
            }
          }

          getProductTotalQtyInReceiptForDelivery = function (currentLine, receipt, deliveryType) {
            var receiptQty = _.reduce(receipt.get('lines').models, function (memo, line, index) {
              if (line.get('id') !== currentLine.get('id') && line.get('product').get('id') === currentLine.get('product').get('id') && line.get('warehouse').id === currentLine.get('warehouse').id && line.get('cUSTDELDeliveryCondition') === deliveryType) {
                if (OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && line.get('product').get('productType') !== 'S') {
                  return OB.DEC.add(memo, line.get('qty'));
                } else {
                  return OB.DEC.add(memo, OB.DEC.Zero);
                }
              } else {
                return OB.DEC.add(memo, OB.DEC.Zero);
              }
            }, OB.DEC.Zero);
            return OB.DEC.add(receiptQty, currentLine.get('qty'));
          }

          getWarehouseDeliveryItemsWarehouse = _.find(OB.MobileApp.model.get('warehouses'), function (warehouse) {
            if (!OB.UTIL.isNullOrUndefined(warehouse.cUSTDELDeliveryType)) {
              return warehouse.cUSTDELDeliveryType === deliveryType;
            }
          });

          args.orderline.set('newDeliveryCondition', deliveryType);
          var productId = args.productToAdd.get('id'),
              qty = getProductTotalQtyInReceiptForDelivery(args.orderline, OB.MobileApp.model.receipt, deliveryType);
          if (!_.isEmpty(stockCheckLocation) && !OB.UTIL.isNullOrUndefined(getWarehouseDeliveryItemsWarehouse)
              && args.productToAdd.get('productType') !== 'S') {
            var process = new OB.DS.Request('com.openbravo.sharaf.retail.deliveryconditions.processes.CheckStockSAPService');
            process.exec({
              productId: productId,
              qty: qty,
              warehouse: getWarehouseDeliveryItemsWarehouse.warehouseid,
              stockLocation: stockCheckLocation
            }, function (data) {
               if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.status) && data.status == 0) {
	              args.orderline.set('cUSTDELDeliveryCondition', args.orderline.get('newDeliveryCondition'));
	              args.orderline.set('isStockAvailable', true);
	              args.orderline.set('cUSTDELDeliveryLocation', getWarehouseDeliveryItemsWarehouse.warehousename);
	              args.orderline.get('warehouse').id = getWarehouseDeliveryItemsWarehouse.warehouseid;
	              args.orderline.get('warehouse').warehousename = getWarehouseDeliveryItemsWarehouse.warehousename;
	              finalCallback();
	            } else {
	              OB.MobileApp.view.waterfall('onShowPopup', {
	                popup: 'CUSTDEL_UI_DeliveryConditionsPopup',
	                args: {
	                  mandatory: true,
	                  selectedModels: [args.orderline],
	                  callback: finalCallback
	                }
	              });
                  if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.exception) && !OB.UTIL.isNullOrUndefined(data.exception.status)
                      && !OB.UTIL.isNullOrUndefined(data.exception.status.data) && !OB.UTIL.isNullOrUndefined(data.exception.status.data.message)) {
                        OB.UTIL.showConfirmation.display(data.exception.status.data.message, [{
                          label: OB.I18N.getLabel('OBMOBC_LblOk')
                        }]);
                  } else {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
                        label: OB.I18N.getLabel('OBMOBC_LblOk')
                     }]);      
                  }	              
	              args.orderline.set('isStockAvailable', false);
	              return;
	           }
            }, function () {
              OB.MobileApp.view.waterfall('onShowPopup', {
                popup: 'CUSTDEL_UI_DeliveryConditionsPopup',
                args: {
                  mandatory: true,
                  selectedModels: [args.orderline],
                  callback: finalCallback
                }
              });
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
                label: OB.I18N.getLabel('OBMOBC_LblOk')
              }]);
              args.orderline.set('isStockAvailable', false);
              return;
            });
          } else if (!_.isEmpty(stockCheckLocation) && !OB.UTIL.isNullOrUndefined(getWarehouseDeliveryItemsWarehouse)
            && args.productToAdd.get('productType') === 'S') {
              args.orderline.set('cUSTDELDeliveryCondition', args.orderline.get('newDeliveryCondition'));
              args.orderline.set('isStockAvailable', true);
              finalCallback();
        } else {
            finalCallback();
          }
        }
      };

      if (negativeQty) {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
      if (args.orderline.get('sHAQUOOrder')) {
        args.orderline.get('product').set('invoice_type', 'New Invoice');
        checkStockInSAP(args, function () {
          OB.SHAQUO.DigitalProductCheckForEpay(OB.SHAQUO.quoData.data, OB.SHAQUO.quoData.idx + 1);
          finalCallback();
        });
      } else {
        var deliveryTypeSK = args.orderline.get('cUSTDELDeliveryCondition'),
            stockLocArr = OB.MobileApp.model.get('sharafStockLocation'),
            sharafDelivery = OB.MobileApp.model.get('sharafDeliveryConditions'),
            deliveryType = "",
            stockCheckLocation = "";
        for (var i = 0; i < sharafDelivery.length; i++) {
          if (deliveryTypeSK === sharafDelivery[i].searchKey) {
            deliveryType = sharafDelivery[i].searchKey;
          }
        }
        for (var i = 0; i < stockLocArr.length; i++) {
          if (stockLocArr[i].organization === OB.MobileApp.model.attributes.terminal.organization$_identifier && deliveryType === stockLocArr[i].deliveryType) {
            stockCheckLocation = stockLocArr[i].stockLocation;
          }
        }

        if (_.isEmpty(stockCheckLocation)) {
          for (var i = 0; i < stockLocArr.length; i++) {
            if (deliveryType === stockLocArr[i].deliveryType && stockLocArr[i].organization === '*') {
              stockCheckLocation = stockLocArr[i].stockLocation;
              break;
            }
          }
        }

        getProductTotalQtyInReceiptForDelivery = function (currentLine, receipt, deliveryType) {
          var receiptQty = _.reduce(receipt.get('lines').models, function (memo, line, index) {
            if (line.get('id') !== currentLine.get('id') && line.get('product').get('id') === currentLine.get('product').get('id') && line.get('warehouse').id === currentLine.get('warehouse').id && line.get('cUSTDELDeliveryCondition') === deliveryType) {
              if (OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && line.get('product').get('productType') !== 'S') {
                return OB.DEC.add(memo, line.get('qty'));
              } else {
                return OB.DEC.add(memo, OB.DEC.Zero);
              }
            } else {
              return OB.DEC.add(memo, OB.DEC.Zero);
            }
          }, OB.DEC.Zero);
          return OB.DEC.add(receiptQty, currentLine.get('qty'));
        }

        getWarehouseDeliveryItemsWarehouse = _.find(OB.MobileApp.model.get('warehouses'), function (warehouse) {
          if (!OB.UTIL.isNullOrUndefined(warehouse.cUSTDELDeliveryType)) {
            return warehouse.cUSTDELDeliveryType === deliveryType;
          }
        });

        args.orderline.set('newDeliveryCondition', deliveryType);
        var productId = args.productToAdd.get('id');
        if (!_.isEmpty(stockCheckLocation) && !OB.UTIL.isNullOrUndefined(getWarehouseDeliveryItemsWarehouse) && args.productToAdd.get('productType') != 'S') {
          args.orderline.set('cUSTDELDeliveryLocation', getWarehouseDeliveryItemsWarehouse.warehousename);
          args.orderline.get('warehouse').id = getWarehouseDeliveryItemsWarehouse.warehouseid;
          args.orderline.get('warehouse').warehousename = getWarehouseDeliveryItemsWarehouse.warehousename;
          args.orderline.set('cUSTDELDeliveryCondition', args.orderline.get('newDeliveryCondition'));
          qty = getProductTotalQtyInReceiptForDelivery(args.orderline, OB.MobileApp.model.receipt, deliveryType);
          var process = new OB.DS.Request('com.openbravo.sharaf.retail.deliveryconditions.processes.CheckStockSAPService');
          process.exec({
            productId: productId,
            qty: qty,
            warehouse: getWarehouseDeliveryItemsWarehouse.warehouseid,
            stockLocation: stockCheckLocation
          }, function (data) {
            if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.status) && data.status == 0) {
              args.orderline.set('isStockAvailable', true);
              finalCallback();
            } else {
              OB.MobileApp.view.waterfall('onShowPopup', {
                popup: 'CUSTDEL_UI_DeliveryConditionsPopup',
                args: {
                  mandatory: true,
                  selectedModels: [args.orderline],
                  callback: finalCallback
                }
              });

              if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.exception) && !OB.UTIL.isNullOrUndefined(data.exception.status)
                   && !OB.UTIL.isNullOrUndefined(data.exception.status.data) && !OB.UTIL.isNullOrUndefined(data.exception.status.data.message)) {
                    OB.UTIL.showConfirmation.display(data.exception.status.data.message, [{
                       label: OB.I18N.getLabel('OBMOBC_LblOk')
                    }]);
              } else {
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
                       label: OB.I18N.getLabel('OBMOBC_LblOk')
                    }]);      
              }
              args.orderline.set('isStockAvailable', false);
              return;
            }
          }, function () {
            OB.MobileApp.view.waterfall('onShowPopup', {
              popup: 'CUSTDEL_UI_DeliveryConditionsPopup',
              args: {
                mandatory: true,
                selectedModels: [args.orderline],
                callback: finalCallback
              }
            });
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
              label: OB.I18N.getLabel('OBMOBC_LblOk')
            }]);
            args.orderline.set('isStockAvailable', false);
            return;
          });
        } else if (!_.isEmpty(stockCheckLocation) && !OB.UTIL.isNullOrUndefined(getWarehouseDeliveryItemsWarehouse)
            && args.productToAdd.get('productType') === 'S') {
              args.orderline.set('cUSTDELDeliveryCondition', args.orderline.get('newDeliveryCondition'));
              args.orderline.set('cUSTDELDeliveryLocation', getWarehouseDeliveryItemsWarehouse.warehousename);
              args.orderline.get('warehouse').id = getWarehouseDeliveryItemsWarehouse.warehouseid;
              args.orderline.get('warehouse').warehousename = getWarehouseDeliveryItemsWarehouse.warehousename;
              args.orderline.set('isStockAvailable', true);
              finalCallback();
        } else {
          finalCallback();
        }
      }
    }
  }, function () {
    finalCallback();
  });

  if (args.orderline && !args.orderline.get('cUSTDELDeliveryCondition')) {
    callbackSuccess();
  } else if (args.orderline && args.orderline.get('qty') < 0 && !args.options.isVerifiedReturn) {
    // TODO: Review when to remove cUSTDELDeliveryCondition
    args.orderline.unset('cUSTDELDeliveryCondition');
    args.orderline.unset('cUSTDELDeliveryTime');
    finalCallback();
  } else if (args.options && args.options.isVerifiedReturn) {
    if (!args.orderline.get('cUSTDELDeliveryTime')) {
      args.orderline.set('cUSTDELDeliveryTime', new Date());
    } else {
      var deliveryTime = new Date(args.orderline.get('cUSTDELDeliveryTime'));
      args.orderline.set('cUSTDELDeliveryTime', deliveryTime);
    }
    finalCallback();
  }
/* else {
    finalCallback();
  }*/
});
