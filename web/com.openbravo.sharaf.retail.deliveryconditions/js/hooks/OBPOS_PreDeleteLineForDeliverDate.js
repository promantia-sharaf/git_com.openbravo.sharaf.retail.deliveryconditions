/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
	  var notDeleteServicearticle = false;
    if (!OB.UTIL.isNullOrUndefined(args.order) ) {
        if(args.selectedLines.length>0){
      	  for(var i = 0; i < args.selectedLines.length; i++){
      	if(!OB.UTIL.isNullOrUndefined(args.selectedLines[i].get('custdelIsdelart')) && !args.selectedLines[i].get('custdelServiceRemove') && args.selectedLines[i].get('custdelIsdelart')){
      		notDeleteServicearticle = true;
      		  }
      	  }
        }
        if(args.selectedLines.length !== args.order.get('lines').length){
        if(notDeleteServicearticle && !args.order.get('isVerifiedReturn')){
    	  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Service Article can't be deleted, To delete Service Article first Need to delete main article");
    	  args.cancelOperation = true;
      }
     }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());