(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_BeforeCustomerAddrSave', function (args, callbacks) {
	  if(args.customerAddr.get('cityName') === ''){
		  OB.UTIL.showError('City field is mandatory and cannot be left empty');
		  args.cancellation = true;
		  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
	  }else{
		  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
	  }
  });
}());