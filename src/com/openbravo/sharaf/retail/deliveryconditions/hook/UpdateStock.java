/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.deliveryconditions.hook;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.OrgWarehouse;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OrderLoaderHook;

import com.openbravo.sharaf.integration.erpob.process.ImportProductStock;
import com.openbravo.sharaf.retail.customdevelopments.OBProductStock;

public class UpdateStock implements OrderLoaderHook {

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice) {
    boolean checkImmediateDeliveryItemsStockInOB = order.getOrganization()
        .isCustdelChecklocalstock();
    if (checkImmediateDeliveryItemsStockInOB) {
      Warehouse iDWarehouse = getImmediateDeliveryWarehouseForOrganization(order.getOrganization());
      for (OrderLine orderLine : order.getOrderLineList()) {
        if (isValidLine(orderLine)) {
          OBProductStock stock = ImportProductStock.getProductStockByProductAndWarehouse(
              orderLine.getProduct(), iDWarehouse);
          if (stock != null) {
            stock.setQuantityOnHand(stock.getQuantityOnHand().subtract(
                orderLine.getDeliveredQuantity()));
            OBDal.getInstance().save(stock);
          }
        }
      }
    }
  }

  private Warehouse getImmediateDeliveryWarehouseForOrganization(Organization organization) {
    final OBCriteria<OrgWarehouse> warehouseCriteria = OBDal.getInstance().createCriteria(
        OrgWarehouse.class);
    warehouseCriteria.add(Restrictions.eq(OrgWarehouse.PROPERTY_ORGANIZATION, organization));
    warehouseCriteria.add(Restrictions.eq(OrgWarehouse.PROPERTY_CUSTDELDELIVERYTYPE, "IM"));
    warehouseCriteria.setMaxResults(1);
    return ((OrgWarehouse) warehouseCriteria.uniqueResult()).getWarehouse();
  }

  private boolean isValidLine(OrderLine orderLine) {
    return orderLine.getCUSTDELDeliveryCondition() != null
        && orderLine.getCUSTDELDeliveryCondition().equals("IM")
        && orderLine.getDeliveredQuantity().signum() == 1
        && !orderLine.getProduct().getProductType().equals("S")
        && orderLine.getCustdisOrderline() == null;
  }
}