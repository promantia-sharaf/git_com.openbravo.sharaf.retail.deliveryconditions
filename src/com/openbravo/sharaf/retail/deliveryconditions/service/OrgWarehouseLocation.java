package com.openbravo.sharaf.retail.deliveryconditions.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.enterprise.OrgWarehouse;
import org.openbravo.model.common.enterprise.Organization;

public class OrgWarehouseLocation extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(OrgWarehouseLocation.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    // TODO Auto-generated method stub
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();
    String orgid = jsonsent.getString("organization");

    try {
      OBContext.setAdminMode(true);
      HashMap<String, List<String>> map = new HashMap<String, List<String>>();
      List<String> imWarehouseList = new ArrayList<>();
      List<String> cdWarehouseList = new ArrayList<>();
      List<String> wdWarehouseList = new ArrayList<>();
      List<String> sdWarehouseList = new ArrayList<>();
      List<String> pWarehouseList = new ArrayList<>();
      List<String> hWarehouseList = new ArrayList<>();
      List<String> cWarehouseList = new ArrayList<>();
      List<String> ddWarehouseList = new ArrayList<>();
      List<String> odWarehouseList = new ArrayList<>();
      List<String> onWarehouseList = new ArrayList<>();

      OBCriteria<OrgWarehouse> obCriteriaOrg = OBDal.getInstance().createCriteria(
          OrgWarehouse.class);
      obCriteriaOrg.add(Restrictions.eq(OrgWarehouse.PROPERTY_ORGANIZATION, OBDal.getInstance()
          .get(Organization.class, orgid)));
      obCriteriaOrg.addOrder(Order.asc(OrgWarehouse.PROPERTY_PRIORITY));

      List<OrgWarehouse> organizationWarehouseList = obCriteriaOrg.list();
      if (!organizationWarehouseList.isEmpty()) {
        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("IM")) {
            String im = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = im.lastIndexOf(")");
            imWarehouseList.add(im.substring(1, idx));
          }
        }
        if (!imWarehouseList.isEmpty()) {
          map.put("IM", imWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("CD")) {
            String cd = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = cd.lastIndexOf(")");
            cdWarehouseList.add(cd.substring(1, idx));
          }
        }
        if (!cdWarehouseList.isEmpty()) {
          map.put("CD", cdWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("WD")) {
            String wd = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = wd.lastIndexOf(")");
            wdWarehouseList.add(wd.substring(1, idx));
          }
        }
        if (!wdWarehouseList.isEmpty()) {
          map.put("WD", wdWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("SD")) {
            String sd = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = sd.lastIndexOf(")");
            sdWarehouseList.add(sd.substring(1, idx));
          }
        }
        if (!sdWarehouseList.isEmpty()) {
          map.put("SD", sdWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("P")) {
            String p = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = p.lastIndexOf(")");
            pWarehouseList.add(p.substring(1, idx));
          }
        }
        if (!pWarehouseList.isEmpty()) {
          map.put("P", pWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("H")) {
            String h = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = h.lastIndexOf(")");
            hWarehouseList.add(h.substring(1, idx));
          }
        }
        if (!hWarehouseList.isEmpty()) {
          map.put("H", hWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("C")) {
            String c = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = c.lastIndexOf(")");
            cWarehouseList.add(c.substring(1, idx));
          }
        }
        if (!cWarehouseList.isEmpty()) {
          map.put("C", cWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("DD")) {
            String dd = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = dd.lastIndexOf(")");
            ddWarehouseList.add(dd.substring(1, idx));
          }
        }
        if (!ddWarehouseList.isEmpty()) {
          map.put("DD", ddWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("OD")) {
            String od = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = od.lastIndexOf(")");
            odWarehouseList.add(od.substring(1, idx));
          }
        }
        if (!odWarehouseList.isEmpty()) {
          map.put("OD", odWarehouseList);
        }

        for (int i = 0; i < organizationWarehouseList.size(); i++) {
          if (organizationWarehouseList.get(i).getCUSTDELDeliveryType() != null
              && organizationWarehouseList.get(i).getCUSTDELDeliveryType().toString()
                  .equalsIgnoreCase("ON")) {
            String on = organizationWarehouseList.get(i).getWarehouse().toString().substring(50);
            int idx = on.lastIndexOf(")");
            onWarehouseList.add(on.substring(1, idx));
          }
        }
        if (!onWarehouseList.isEmpty()) {
          map.put("ON", onWarehouseList);
        }
      }

      data.put("WarehouseLocation", map);
      result.put("status", 0);
      result.put("data", data);
    } catch (Exception e) {
      log.error("Error getting warehouse location for Organization: " + e.getMessage(), e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

}
