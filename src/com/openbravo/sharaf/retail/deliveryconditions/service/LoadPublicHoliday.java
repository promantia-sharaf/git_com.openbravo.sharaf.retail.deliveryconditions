package com.openbravo.sharaf.retail.deliveryconditions.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class LoadPublicHoliday extends ProcessHQLQuery {
  private static Logger log = Logger.getLogger(LoadPublicHoliday.class);

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    List<String> hqlQueries = new ArrayList<String>();

    String hqlQry = "SELECT a.id as id,a.holidayDescription as description,a.custdelSkipdeliverydates.id as headerID, a.organization.id as organization,a.dateofholiday as dateOfHoliday, a.active as active "
        + "FROM custdel_pubicholiday a " + "WHERE a.active = 'Y' " + "AND a.client = '"
        + OBContext.getOBContext().getCurrentClient().getId() + "'";

    // Add the query with the named parameter to the list
    hqlQueries.add(hqlQry);
    log.info("load public holiday" + hqlQueries);

    return hqlQueries;
  }

}