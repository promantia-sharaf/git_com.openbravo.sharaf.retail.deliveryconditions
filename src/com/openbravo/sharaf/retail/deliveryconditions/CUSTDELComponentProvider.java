/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.deliveryconditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

@ApplicationScoped
@ComponentProvider.Qualifier(CUSTDELComponentProvider.QUALIFIER)
public class CUSTDELComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CUSTDEL_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.openbravo.sharaf.retail.deliveryconditions";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    String[] resourceList = { "components/actionButtonDeliveryCondition",
        "components/popupDeliveryCondition", "hooks/postAddProductToOrderHook",
        "hooks/preShowPaneHook", "hooks/renderOrderLineHook", "model/addProductProperty",
        "utils/custdelUtils", "hooks/preOrderSaveHook", "hooks/beforeCustomerAddrSaveHook",
        "hooks/beforeCustomerSaveHook", "model/extendPropertyCategory",
        "hooks/OBPOS_PreDeleteLineForDeliverDate", "model/preferredDeliveryDateProperty",
        "model/publicHolidayDates", "hooks/prePaymentForSD" };

    for (String resource : resourceList) {
      globalResources.add(createComponentResource(ComponentResourceType.Static,
          prefix + resource + ".js", POSUtils.APP_NAME));
    }

    return globalResources;
  }
}
