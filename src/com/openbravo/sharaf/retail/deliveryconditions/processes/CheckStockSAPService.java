/*
 ************************************************************************************
 * Copyright (C) 2018-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.deliveryconditions.processes;

import java.math.BigDecimal;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;

import com.openbravo.sharaf.retail.sapws.stock.StockSAPService;

public class CheckStockSAPService extends BaseCheckStock {
  private static Logger log = Logger.getLogger(CheckStockSAPService.class);
  @Inject
  private StockSAPService stockService;

  @Override
  protected BigDecimal getStock(Product product, Warehouse warehouse, String stockLocation) {
    log.info("Stock SAP service");
    log.info("product" + product.getId());
    log.info("warehouse" + warehouse.toString());
    log.info("stockLocation" + stockLocation.toString());
    if (warehouse.getSapobmpSapCode() == null || warehouse.getSapobmpSapCode().isEmpty()) {
      return BigDecimal.valueOf(-1L);
    }
    log.info("warehouse sapcode" + warehouse.getSapobmpSapCode());
    log.info("product id" + product.getSearchKey());

    BigDecimal stock = stockService.getStock(product.getSearchKey(), warehouse.getSapobmpSapCode(),
        stockLocation);
    log.info("stock" + stock.toString());
    if (stock == null) {
      return BigDecimal.ZERO;
    }
    return stock;
  }
}
