/*
 ************************************************************************************
 * Copyright (C) 2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.deliveryconditions.processes;

import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseCheckStock extends JSONProcessSimple {

  protected abstract BigDecimal getStock(Product product, Warehouse warehouse, String stockLoc);

  private static final Logger log = LoggerFactory.getLogger(BaseCheckStock.class);

  public JSONObject exec(JSONObject jsonInfo) throws OBException {
    OBContext.setAdminMode(true);
    try {
      String stockLoc = "";
      JSONObject parameters = jsonInfo.optJSONObject("parameters");
      String productId = parameters.optJSONObject("productId").optString("value");
      String warehouseId = parameters.optJSONObject("warehouse").optString("value");
      Long qty = parameters.optJSONObject("qty").optLong("value");
      if (parameters.optJSONObject("stockLocation") != null) {
        stockLoc = parameters.optJSONObject("stockLocation").optString("value");
      }

      Product product = OBDal.getInstance().get(Product.class, productId);
      Warehouse warehouse = OBDal.getInstance().get(Warehouse.class, warehouseId);

      BigDecimal stock = getStock(product, warehouse, stockLoc);

      JSONObject response = new JSONObject();
      try {
        JSONObject status = new JSONObject();
        if (stock.compareTo(new BigDecimal(qty)) >= 0) {
          status.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
          response.put(JsonConstants.RESPONSE_DATA, status);
          response.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
        } else if (stock.compareTo(new BigDecimal(-1L)) == 0) {
          status.put("message",
              "Warehouse SAP code is Empty for Stock Check. Please contact PoS Support");
          log.error("Warehouse SAP code is Empty for Stock Check. Please contact PoS Support");
          status.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
          response.put(JsonConstants.RESPONSE_DATA, status);
          response.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        } else {
          status.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
          response.put(JsonConstants.RESPONSE_DATA, status);
          response.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        }
      } catch (JSONException ignore) {
      }
      return response;
    } catch (Exception e) {
      log.error("Error", e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}