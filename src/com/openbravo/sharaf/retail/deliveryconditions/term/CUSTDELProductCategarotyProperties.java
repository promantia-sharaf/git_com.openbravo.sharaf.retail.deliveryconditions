/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.deliveryconditions.term;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.Category;

@Qualifier(Category.productCategoryPropertyExtension)
public class CUSTDELProductCategarotyProperties extends ModelExtension {
	  public static final Logger log = Logger.getLogger(CUSTDELProductCategarotyProperties.class);

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
	  log.info("inside java classssssssssssssssss");
    return Arrays.asList(new HQLProperty("pCat.custdelDeliservArti.id",
        "custdelDeliservArti"));
  }
}