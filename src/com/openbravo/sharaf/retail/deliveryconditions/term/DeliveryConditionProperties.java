/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.deliveryconditions.term;

import java.util.Arrays;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.retail.posterminal.term.QueryTerminalProperty;

public class DeliveryConditionProperties extends QueryTerminalProperty {

  @Override
  protected boolean isAdminMode() {
    return true;
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    String language = OBContext.getOBContext().getLanguage().getId();
    return Arrays.asList(new String[] { "select list.searchKey as searchKey, coalesce("
        + " (select trl.name from list.aDListTrlList trl where  trl.language.id = '" + language
        + "'), list.name) as name from ADList list "
        + "where list.reference.id = '4E81AD2CC0924D61A0608C725E8B9E94' "
        + " and list.$readableSimpleCriteria and list.$activeCriteria "
        + "order by list.sequenceNumber asc" });
  }

  @Override
  public String getProperty() {
    return "sharafDeliveryConditions";
  }

  @Override
  public boolean returnList() {
    return false;
  }
}
