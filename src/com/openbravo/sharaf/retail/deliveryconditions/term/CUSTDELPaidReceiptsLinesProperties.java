/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.deliveryconditions.term;

import java.util.Arrays;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class CUSTDELPaidReceiptsLinesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    return Arrays.asList(new HQLProperty("ordLine.cUSTDELDeliveryCondition",
        "cUSTDELDeliveryCondition"),
        new HQLProperty("ordLine.warehouse", "cUSTDELDeliveryLocation"), new HQLProperty(
            "ordLine.cUSTDELDeliveryTime", "cUSTDELDeliveryTime"),
        new HQLProperty("ordLine.custdelIsdelart", "custdelIsdelart"));
  }
}